/*
 |
 | ReplaceALL
 |
 */
String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};
/*
 |
 | Remove Whitespace
 |
 */
function remove_spacing(input)
{
    if (input)
    {
        return input.replace(/ /g, "");
    }
    return '';
}

/*
 |------------------------------------------------
 | Load script source.
 |------------------------------------------------
 |
 */
function loadScript(url, callback)
{
    // Adding the script tag to the head as suggested before
    let src = $('[src="' + url + '"]')
    if (src.length < 1)
    {
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    } else {

        callback.call();
    }
}

/*
 |------------------------------------------------
 | Jquery can search element by pattern of their attribute.
 |------------------------------------------------
 |
 |$('elm:attrNameStart(ng)')
 */
$.extend($.expr[':'], {
    attrNameStart: function (el, i, props) {

        var hasAttribute = false;

        $.each(el.attributes, function (i, attr) {
            if (attr.name.indexOf(props[3]) !== -1) {
                hasAttribute = true;
                return false;  // to halt the iteration
            }
        });

        return hasAttribute;
    }
})

/*
 |------------------------------------------------
 | add prototype array diff into Array object.
 |------------------------------------------------
 */
Array.prototype.diff = function (arr2) {
    var ret = [];
    for (var i in this) {
        if (arr2.indexOf(this[i]) > -1) {
            ret.push(this[i]);
        }
    }
    return ret;
};
// -----------------------------------------------

/*
 |------------------------------------------------
 | add prototype array remove into Array object.
 |------------------------------------------------
 */
Array.prototype.remove = function (index) {
    this.splice(index, 1)
};
// -----------------------------------------------

/*
 |------------------------------------------------
 | add prototype array get index.
 |------------------------------------------------
 */
Array.prototype.get_index = function (object_name, value) {
    return this.map((res) => {
        return res[object_name]
    }).indexOf(value)
};

// -----------------------------------------------

/*
 |------------------------------------------------
 | Create uniqueid based on time.
 |------------------------------------------------
 */
function unique_id()
{
    var uniq = new Date().valueOf();
    return uniq;
}
// ----------------------------------------------

/*
 |------------------------------------------------
 | Execution function by name.
 |------------------------------------------------
 |
 | executeFunctionByName('helloWorld', window);
 */
function executeFunctionByName(functionName, context /*, args */) {

    var args = [].slice.call(arguments).splice(2),
            namespaces = functionName.split("."),
            func = namespaces.pop();

    for (var i = 0; i < namespaces.length; i++) {
        context = context[namespaces[i]];
    }

    return context[func].apply(this, args);
}
// ----------------------------------------------

/*
 |------------------------------------------------
 | Remove item in an array.
 |------------------------------------------------
 |
 | var a = [a,b,c,d,e]
 | removeArray(a, index)
 */
function removeArray(array, index)
{
    array.splice(index, 1)
}

/*
 |------------------------------------------------
 | Set pop up will appears at center of screen.
 |------------------------------------------------
 |
 | popUpCenter('http://www.google.com', 'Google', 100, 200, 'other,parameters', {}  )
 */
function popupCenter(url, title, w, h, other, data) {
    // Fixes dual-screen position                         Most browsers      Firefox
    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;
    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left + ',scrollbars,' + other);

    if (data)
    {
        var form = '<form method="post" action="" target="TheWindow">';
    }
    // Puts focus on the newWindow
    if (window.focus) {
        newWindow.focus();
    }
    return newWindow;
}

/*
 |------------------------------------------------
 | Class Sort arrays.
 |------------------------------------------------
 |
 |   var a = [{alphabet:'c'},{alphabet:'a'},{alphabet:'e'}]
 |   var b = [2,1,3,4,5,6]
 |   var s = new sortBy(b,'-'); // [6,5,4,3,2,1]
 |   var s = new sortBy(b); // [1,2,3,4,5,6]
 |   var s = new sortBy(a,'alphabet', 's'); // [{alphabet:'a'},{alphabet:'c'},{alphabet:'e'}]
 |   var s = new sortBy(a,'-alphabet', 's'); // [{alphabet:'e'},{alphabet:'c'},{alphabet:'a'}]
 */
function sortBy(objs, prop, typeProp)
{
    var $this = this;
    $this.property = '';
    $this.type = 's';
    $this.sortOrder = 1;
    function set_sort_property(prop) {
        console.log(prop)
        if (prop[0] === "-") {
            $this.sortOrder = -1;
            prop = prop.substr(1);
        }
        $this.property = prop;
    }
    function get_sort_property() {
        return $this.property;
    }
    function set_sort_type(settype) {
        $this.type = settype ? settype : 's';
    }
    function get_sort_type() {
        return $this.type;
    }

    function compare(a, b) {
        var sortOrder = 1;
        var prop = get_sort_property()
        if (prop.length > 0)
        {
            switch (get_sort_type())
            {
                case 'i':
                    a[prop] = parseInt(a[prop]);
                    b[prop] = parseInt(b[prop]);
                    break;
                case 'b':
                    a[prop] = JSON.parse(a[prop]);
                    b[prop] = JSON.parse(b[prop]);
                    break;
                case 's':
                default:
                    a[prop] = String(a[prop]);
                    b[prop] = String(b[prop]);
                    break;
            }
            var result = (a[prop] < b[prop]) ? -1 : (a[prop] > b[prop]) ? 1 : 0;
        } else {
            switch (get_sort_type())
            {
                case 'i':
                    a = parseInt(a);
                    b = parseInt(b);
                    break;
                case 'b':
                    a = JSON.parse(a);
                    b = JSON.parse(b);
                    break;
                case 's':
                default:
                    a = String(a);
                    b = String(b);
                    break;
            }
            var result = (a < b) ? -1 : (a > b) ? 1 : 0;
        }
        return result * sortOrder;
    }
    if (prop)
        set_sort_property(prop);
    if (typeProp)
        set_sort_type(typeProp);
    $this.sortObj = objs.sort(compare);
    return $this.sortObj;

}
// -----------------------------------------------------------------

/*
 |------------------------------------------------
 | Copy text from textarea / string direct copy.
 |------------------------------------------------
 |
 | copy('textarea', callback())
 | copy('lorem ipsum dor amet...', callback())
 */
function copy()
{
    var params = arguments;
    var paramsLength = arguments.length;
    var e = params[0];
    var is_e = params[1] == false ? false : true;
    var c = params[paramsLength - 1];
    console.log(c)
    if (!is_e)
    {
        var uniqueid = unique_id();
        var textareaId = "__temp_textarea__" + uniqueid;
        var element = $("<textarea class='sr-only' id='" + textareaId + "'></textarea>");
        element.appendTo('body')
                .each(function () {
                    $(this).val(e)
                    $(this).select()
                })
    } else
    {
        $(e).select();
    }

    try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        console.log('Copying text was ' + msg);
        if (!is_e)
        {
            $(this).remove();
        }
        if (typeof c == 'function')
        {
            c()
        }
    } catch (err) {
        console.error('Oops, unable to copy', err);
    }
}
// -----------------------------------------------------------------

/*
 |------------------------------------------------
 | Check if the passed str is JSON string or not.
 |------------------------------------------------
 |
 | isJSON('foo') // false
 | isJSON('{"json":true}') // true
 */
function isJSON(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
JSON.isJSON = function (str) {
    return isJSON(str)
};

function formdata_builder(data, fData)
{
    // set and detect FormData constructor
    fData = fData && fData.constructor == FormData ? fData : new FormData();

    // jika data exist dan data.constructor bukan formdata
    if (data && data.constructor != FormData)
    {
        $.each(data, function (a, b) {
            // Jika b bukan sebuah object file, dan didalam b ada object, gunakan stringify // Object File tidak boleh di stringify
            if (b.constructor != File)
            {
                if (typeof b == 'object')
                {
                    var length = b.constructor == Array ? b.length : Object.keys(b).length;
                    console.log(length, b)
                    b = length > 0 ? JSON.stringify(b) : b;
                    fData.append(a, b);
                }
            }
        })
    } else
    {
        fData = data
    }
    return fData;
}
// -----------------------------------------------------------------

/*
 |
 | Ajax Sender
 |
 */
function ajax(options)
{

    var deff = $.Deferred(),
            fData = new FormData();
    _watch_fData = [];

    options = $.extend({
        upload_progress: function (event, percentage)
        {

        },
        download_progress: function (event, percentage)
        {

        },
        isFile: false,
        cache: true,
        contentType: 'application/x-www-form-urlencoded', // set false if want to upload file
        processData: true, // set false if want to upload file
        type: 'POST',
        useFormData: true,
        data: {}
    }, options);

    if (!options.url)
    {
        alert('no url given');
        return false;
    }

    if (options.type == 'POST' && options.data.constructor != FormData && options.useFormData)
    {

        $.each(options.data, function (a, b) {
            if (b)
            {
                // Jika b bukan sebuah object file, dan didalam b ada object, gunakan stringify // Object File tidak boleh di stringify
                if (b.constructor != File)
                {
                    b = typeof b == 'object' ? JSON.stringify(b) : b;
                }
                fData.append(a, b);
                _watch_fData.push(a)
            }
        })
        options.data = _watch_fData.length < 1 ? {} : fData;
    }

    options.success = function (data) {
        deff.resolve(data);
    }
    options.error = function (data)
    {
        deff.reject(data);
    }
    return $.ajax(options)
}

/*
 |
 | HTML decode and encode;
 |
 */
function html_encode(value)
{
    return $('<div/>').text(value).html();
}

function html_decode(value)
{
    return $('<div/>').html(value).text();
}
// ============================================================
function range(input, total, data)
{
    var arr = []
    total = parseInt(total);
    for (var i = input; i < total; i++) {
        o = data ? data[i] : i
        if (o)
        {
            arr.push(o);
        }
    }

    return arr;
}
function getRandomInt(min, max) {
    if (Array.isArray(min))
    {
        var minLen = 0;
        var maxLen = min.length - 1;
        var index = Math.floor(Math.random() * (maxLen - minLen + 1)) + minLen; //The maximum is exclusive and the minimum is inclusive
        return min[index];
    } else
    {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is exclusive and the minimum is inclusive
    }
}

// manipulation url
(function ($) {

    $.URL_DECODE = function (address)
    {
        return URL_DECODE.get(address)
    }


}(jQuery));

var URL_DECODE = (function () {

    var o = function () {}
    o.prototype =
            {
                get: function (string) {
                    string = (string) ? $('<a>', {href: string})[0] : $('<a>', {href: document.URL})[0]

                    var isQuery = string.href.match(/\?(.*)/) == null ? false : true;

                    var u = {
                        hash: {},
                        title: document.title,
                        hashRaw: string.hash,
                        queryRaw: string.search,
                        query: {},
                        origin: string.origin,
                        href: string.href,
                        host: string.host,
                        port: (string.port) ? string.port : 80,
                        protocol: string.protocol,
                        hostname: string.hostname,
                        pathname: string.pathname,
                        access_url: string.origin + string.port + string.pathname,
                        hashArray: []
                    },
                            url = (url === undefined) ? document.URL : url;

                    if (u.queryRaw !== '') {

                        var uQuery = u.queryRaw.substr(1)

                        $.each(uQuery.split('&'), function (a, b) {
                            var qName = b.match(/.*?(?=:)|.*?(?=\=)/);
                            qName = (qName) ? qName : b;
                            var qVal = (b.match(/=(.*)/) !== null) ? b.match(/=(.*)/)[1] : '';
                            qName = Array.isArray(qName) == true ? qName[0] : qName;
                            u.query[qName] = qVal;
                        })

                    }

                    if (u.hashRaw !== '') {

                        var uHash = u.hashRaw.match(/\#(.*)/),
                                uHRaw = uHash[0];

                        u.hashData = uHash[1];
                        $.each(u.hashData.split('&'), function (a, b) {
                            if (b !== '' && b !== undefined && b !== null) {
                                var hName = (b.match(/.*?(?=:)|.*?(?=\=)/) == null) ? b : b.match(/.*?(?=:)|.*?(?=\=)/);
                                var hVal = (b.match(/=(.*)/) !== null) ? b.match(/=(.*)/)[1] : undefined;
                                hName = Array.isArray(hName) == true ? hName[0] : hName;
                                u.hash[hName] = hVal;
                                u.hashArray.push(b)
                            }
                        })
                    }

                    return u;
                },
                params: function (params, address, config) {
                    var obj = {}, i, parts, len, key, value, uri = URL.get(address);

                    config = $.extend({
                        extend: true
                    }, config)

                    if (typeof params === 'string') {
                        value = location.search.match(new RegExp('[?&]' + params + '=?([^&]*)[&#$]?'));
                        return value ? value[1] : undefined;
                    }

                    if (config.extend == true)
                    {
                        var _params = location.search.substr(1).split('&');
                        for (i = 0, len = _params.length; i < len; i++) {
                            parts = _params[i].split('=');
                            if (!parts[0]) {
                                continue;
                            }
                            obj[parts[0]] = parts[1] || true;
                        }
                    }

                    obj = $.extend(uri.query, obj)

                    if (typeof params !== 'object') {
                        return obj;
                    }

                    for (key in params) {
                        value = params[key];
                        if (typeof value === 'undefined') {
                            delete obj[key];
                        } else {
                            obj[key] = value;
                        }
                    }

                    parts = [];
                    for (key in obj) {
                        if (key)
                        {
                            parts.push(key + (obj[key] === true ? '' : '=' + obj[key]));
                        }
                    }

                    return URL.get(address).access_url + '?' + parts.join('&');
                },
                add:
                        {

                        },
                update:
                        {
                            remove: {
                                hash: function (objHash)
                                {
                                    var hash = URL.get().hash;
                                }
                            },
                            build:
                                    {
                                        hash: function (objHash)
                                        {
                                            var hash = [];
                                            $.each(objHash, function (a, b) {
                                                var val = (b) ? '=' + b : '';
                                                hash.push(a + val);
                                            })
                                            return hash.join('&');
                                        }
                                    }
                        }
            }

    return new o();

})()

function make_slug(text)
{
    if (text)
    {
        var str = text.toLowerCase();
        str = str.replace(/[^a-z0-9]+/g, '-');
        str = str.replace(/^-|-$/g, '');
        return str;
    }
}

// read image
function readFileAsDataUrl(file) {
    var deff = $.Deferred();
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        deff.resolve(this)
    }, false);

    reader.readAsDataURL(file);
    return $.when(deff.promise());
}


$(document).ready(function () {

    // EVENT READER
    $('[ng-truncate]').each(function (e) {
        var a = ['div', 'span', 'i', 'p']
        var context = $(this)[0];
        var text = '';
        if (a.indexOf(context.localName) > -1)
        {
            text = context.textContent;
        }
        var maxLength = $(this).attr('length');
        maxLength = maxLength ? maxLength : 50;

        if (text.length > maxLength) {
            // split the text in two parts, the first always showing
            var firstPart = String(text).substring(0, maxLength);
            var secondPart = String(text).substring(maxLength, text.length);
            var withdot = firstPart + '...';

            // create some new html elements to hold the separate info
            // var firstSpan   = $compile('<span>' + firstPart + '</span>')(scope);
            // var readMoreLink = '<br><a href="#/open/article/'+data.id_article+'">Read more</a> '

            $(this).html(withdot);

            // remove the current contents of the element
            // and add the new ones we created
        }

    })
    $('[ng-load]').on('load', function (e) {
    })

    $(document).bind('DOMSubtreeModified', function () {
        // console.log("now there are " + $('[ng-load]').length + " links on this page.");
    })
})

// on keypress / shortcut mode
window.key = []
var Shortcut = function (options, callback)
{
    var arr_code =
            {
                0: 48,
                1: 49,
                2: 50,
                3: 51,
                4: 52,
                5: 53,
                6: 54,
                7: 55,
                8: 56,
                9: 57,

                a: 65,
                b: 66,
                c: 67,
                d: 68,
                e: 69,
                f: 70,
                g: 71,
                h: 72,
                i: 73,
                j: 74,
                k: 75,
                l: 76,
                m: 77,
                n: 78,
                o: 79,
                p: 80,
                q: 81,
                r: 82,
                s: 83,
                t: 84,
                u: 85,
                v: 86,
                w: 87,
                x: 88,
                y: 89,
                z: 90,
                shift: 16,
                enter: 13,
                backspace: 8,
                space: 32,
                left: 37,
                top: 38,
                right: 39,
                bottom: 40,
                tab: 9,
                capslock: 20,
                leftwindow: 91,
                rightwindow: 92,
                // ctrl: 17,
                // alt: 18,
                rightClickKey: 93,
                esc: 27,
                printScreen: 44,
                scrollLock: 145,
                break: 19,
                pause: 19,
                numLock: 144,
                '/': 111,
                '*': 106,
                '-': 109,
                '+': 107,
                num0: 96,
                num1: 97,
                num2: 98,
                num3: 99,
                num4: 100,
                num5: 101,
                num6: 102,
                num7: 103,
                num8: 104,
                num9: 105,
                numDelete: 110,
                insert: 45,
                home: 36,
                pageUp: 33,
                delete: 46,
                end: 35,
                pageDown: 34,
                '`': 192,
                '-': 189,
                '=': 187,
                '[': 219,
                ']': 221,
                // '\\': 220,
                ';': 186,
                "'": 222,
                ',': 188,
                '.': 190,
                '/': 191,
            }
    var _deff_option = {
        target: undefined,
        command: undefined,
        trigger: 'keyup',
        callback: function () {}
    }
    var $this = this;
    $this.options = $.extend(_deff_option, options);

    if (!$this.options.target || !$this.options.command)
    {
        alert('Error on creating Shortcut ' + $this.options.command);
        return false;
    }

    var fn = arguments;
    $this.options.command = $this.options.command.toLowerCase();
    var keyboard = $this.options.command.split('+');
    var ctrlIndex = keyboard.indexOf('ctrl');
    var altIndex = keyboard.indexOf('alt');
    var isCtrl = ctrlIndex > -1 ? true : false;
    var isAlt = altIndex > -1 ? true : false;

    if (isCtrl) {
        removeArray(keyboard, ctrlIndex);
    }
    if (isAlt) {
        removeArray(keyboard, altIndex);
    }


    $(document).delegate($this.options.target, $this.options.trigger, function (e) {
        var key = e.key.toLowerCase();
        if (key == keyboard[0])
        {
            e.preventDefault()
            if (typeof callback == 'function')
            {
                callback(e);
            }
        }
    });
}



function write_data(options)
{
    var dataText, promises = [];
    options = $.extend({
        target: undefined,
        records: [],
        template: '',
        overwrite: true,
        typeWrite: 'append', //[append, prepend]
        success: function (event, ui, data) { },
        beforeWrite: function (event, target, data)
        {

        },
        beforeAppend: function (event, target, data)
        {

        },
        afterAppend: function (event, target, data)
        {

        }
    }, options)
    /*when option records given*/
    $.when(options.records).done(function (response) {

        // create new event;
        var event = new Event('write');

        // function before write
        options.beforeWrite(event, options.target, options.records);
        if (options.overwrite)
        {
            $(options.target).html('')
        }
        $.each(response, function (a, b) {
            var dataCustom = {},
                    deff = $.Deferred();

            if (typeof options.dataCustom == 'function')
            {
                dataCustom = options.dataCustom(b);

            }

            b = $.extend({}, b, dataCustom);

            /*stream number is number that automatically create such as 1,2,3,4,5,6,..,n*/
            var pattern,
                    renderTemplete = options.template;

            renderTemplete = renderTemplete.replace(/\(:__stream_number\)/, a + 1);

            $.each(b, function (c, d) {

                pattern = '::' + c + '::';
                var reg = new RegExp(pattern, 'g');
                var value = (b[c]) ? b[c] : '';
                renderTemplete = renderTemplete.replace(reg, value);

            })

            // renderTemplete = $(renderTemplete);

            $(renderTemplete).each(function () {

                deff.resolve({ui: this, options: options});

                promises.push(deff);

                options.beforeAppend(event, this, b)

                options.success(event, this, b)

            })

            if (options.target)
            {
                // $(renderTemplete).appendTo(options.target).each(function(){

                //         $(this).data(b)
                //         componentHandler.upgradeAllRegistered()
                //         options.afterAppend(event, this, b)
                //     })

                if (options.typeWrite == 'append')
                {

                    $(renderTemplete).appendTo(options.target).each(function () {

                        $(this).data(b)
                        if (typeof componentHandler !== 'undefined')
                        {
                            componentHandler.upgradeAllRegistered()
                        }
                        options.afterAppend(event, this, b)
                    })
                } else if (options.typeWrite == 'prepend')
                {
                    $(renderTemplete).prependTo(options.target).each(function () {

                        $(this).data(b)
                        if (typeof componentHandler !== 'undefined')
                        {
                            componentHandler.upgradeAllRegistered()
                        }
                        options.afterAppend(event, this, b)
                    })
                }

            }

            /*else
             {
             dataText += renderTemplete;
             }*/

        })
    })

    return $.when.apply(undefined, promises).promise();
    // return $.when(deff.promise());
}

let ModalFolar = function (options)
{
    let defopt = {
        id: '#folar-modal-lg',
        url: '',
        data: {},
        title: "default title",
        content: '',
        autoInit: false
    }
    options = $.extend(defopt, options)
    this.options = options;
    this.e = $(this.options.id);
    if (options.autoInit)
    {
        this.initialize();
    }
    return this;
}
ModalFolar.prototype = {
    modal: function ()
    {
        return $(this.options.id).modal;
    },
    title: function (title)
    {
        title = title ? title : this.options.title
        this.e.find('.modal-title').html(title)
        return this;
    },
    content: function (content)
    {
        this.e.find('.modal-body').html('<div style="display:flex; justify-content:center; align-items:center; width:100%; height:400px;"><i class="fa fa-circle-o-notch fa-spin"><i></div>')
        var deff = $.Deferred();
        if (content || this.options.content)
        {
            content = $(content);
            content.appendTo(this.e.find('.modal-body')).each(function (res) {
                deff.resolve(res);
            })
        } else if (this.options.url)
        {
            let self = this
            this.e.find('.modal-body').load(this.options.url, this.options.data, function (res) {
                self.e.find('.modal-body').data(self.options.data)
                deff.resolve(self.prototype, res)
            })
        }

        return $.when(deff.promise());
    },
    refresh: () => {
        console.log(this.e)
    },
    initialize()
    {
        window.$ModalFolar = {
            data: this.options,
            target: this.e,
            refresh: () => {
                this.e.find('.modal-body').load(this.options.url, this.options.data, function (res) {
                })
            }
        }
        this.title()
        return this.content()
    }
}

$(document).delegate('[data-folar-modal]', 'click', function (e) {
    e.preventDefault();
    let data = $(this).data();
    if (data.modalSingle == true)
    {
        $('.modal').modal('hide')
    }
    let url = $(this).attr('href');
    let title = $(this).attr('title');
    let id = $(this).attr('data-folar-modal');
    id = id ? id : '#folar-modal-lg';

    let popup = new ModalFolar({
        url: url,
        title: title,
        id: id
    });

    popup.initialize()
            .done(function () {
                if (data.pushstate)
                {
                    history.pushState(data, "", url);
                }
                $(id).data(data)
            })
    popup.e.modal('show')

    $(id).one('hide.bs.modal', function (e) {
        $('body').css('padding-right', '')
        let data = $(this).data();
        if (data.pushstate)
        {
            history.back();
        }
    })
})

function nominalToPercent(nominal, total)
{
    return ((nominal / total) * 100).toFixed(1);
}

function percentToNominal(percent, total)
{
    return (total * (percent / 100)).toFixed(0);
}

function intToIDR(angka, use_decimal)
{
    let decimal = '';
    if(use_decimal)
    {
        decimal = get_decimal(angka);
        console.log(decimal);
        decimal = IDRtoInt(decimal);
        if(decimal > 0)
        {
            decimal = ','+decimal;
        }else
        {
            decimal = '';
        }
    }
    angka = IDRtoInt(angka)
    console.log(angka, decimal)
    var rev = parseInt(angka, 10).toString().split('').reverse().join('');
    var rev2 = '';
    for (var i = 0; i < rev.length; i++) {
        rev2 += rev[i];
        if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
            rev2 += '.';
        }
    }
    angka = rev2.split('').reverse().join('')+decimal;
    return angka

}

function  IDRtoInt(angka,use_decimal)
{

    angka = angka ? angka.toString() : '0';
    let decimal = '';
    if(use_decimal)
    {
        decimal = angka.replace(/[-+]?[0-9]*\.?[0-9]+/, '');
    }
    angka = parseInt(angka.replace(/,.*|\D/g, ''), 10)+decimal;
    return angka;
}

function get_decimal(angka)
{
    angka = angka.toString();
    return angka.replace(/[-+]?[0-9]*\.?[0-9]+/, '');
}

function get_initial_outlet_name(name, join)
{
    let nameArr = name.replace(/[^A-Za-z0-9]/g, ' ').split(' ');
    let initialName = [];
    $.each(nameArr, (i, val) => {
        initialName.push(val[0])
    })
    if (join)
    {
        return initialName.join(join);
    }
    return initialName;
}

function lead_zero(value, length)
{
    length = length || 5;
    value = value.toString();
    let val_len = value.length;
    if (val_len < length)
    {
        let chunk = length - val_len;
        let text = '';
        for (var i = 0; i < chunk; i++) {
            text += '0';
        }
        return text + value;
    }
    return value;

}

function characterlength(element)
{
    let e = $(element);
    let len = e.attr('maxLength');
    let val = e.val();
    let vallen = val.length
    e.addClass('characterlength')
    $(element + ':after').css('content', vallen + '/' + len)
    $(document).delegate(element, 'keyup', function () {
        $(element + ':after').css('content', vallen + '/' + len)
    })
}

function trigger(e, trigger_value)
{
    $(e).trigger(trigger_value)
}

$(document).delegate('[type="number"][maxvalue]', 'keypress change input', function () {
    let val = parseFloat($(this).val());
    let allow = parseFloat($(this).attr('maxvalue'))
    if (val > allow)
    {
        $(this).val(allow)
    }
})

window.audio_notif = new Audio(base_url() + 'assets/music/notification.mp3');
function ring_notif()
{
    window.audio_notif.play();
}

// Geolocation
/*!
 * A simple jQuery Wrapper for Geolocation API
 * Supports Deferreds
 *
 * @author: Manuel Bieh
 * @url: http://www.manuel-bieh.de/
 * @documentation: http://www.manuel-bieh.de/blog/geolocation-jquery-plugin
 * @version 1.1.0
 * @license MIT
 */

(function ($) {

    $.extend({

        geolocation: {

            watchIDs: [],

            get: function (arg1, arg2, arg3) {

                var o = {};

                if (typeof arg1 === 'object') {
                    o = $.geolocation.prepareOptions(arg1);
                } else {
                    o = $.geolocation.prepareOptions({success: arg1, error: arg2, options: arg3});
                }

                return $.geolocation.getCurrentPosition(o.success, o.error, o.options);

            },

            getPosition: function (o) {
                return $.geolocation.get.call(this, o);
            },

            getCurrentPosition: function (arg1, arg2, arg3) {

                var defer = $.Deferred();

                if (typeof navigator.geolocation != 'undefined') {

                    if (typeof arg1 === 'function') {

                        navigator.geolocation.getCurrentPosition(arg1, arg2, arg3);

                    } else {

                        navigator.geolocation.getCurrentPosition(function () {
                            defer.resolveWith(this, arguments);
                        }, function () {
                            defer.rejectWith(this, arguments);
                        }, arg1 || arg3);

                        return defer.promise();

                    }

                } else {

                    var error = {"message": "No geolocation available"};

                    if (typeof arg2 === 'function') {
                        arg2(error);
                    }

                    defer.rejectWith(this, [error]);
                    return defer.promise();

                }

            },

            watch: function (o) {

                o = $.geolocation.prepareOptions(o);
                return $.geolocation.watchPosition(o.success, o.error, o.options);

            },

            watchPosition: function (success, error, options) {

                if (typeof navigator.geolocation !== 'undefined') {

                    watchID = navigator.geolocation.watchPosition(success, error, options);
                    $.geolocation.watchIDs.push(watchID);
                    return watchID;

                } else {

                    error();

                }

            },

            stop: function (watchID) {

                if (typeof navigator.geolocation != 'undefined') {
                    navigator.geolocation.clearWatch(watchID);
                }

            },

            clearWatch: function (watchID) {
                $.geolocation.stop(watchID);
            },

            stopAll: function () {

                $.each(jQuery.geolocation.watchIDs, function (key, value) {
                    $.geolocation.stop(value);
                });

            },

            clearAll: function () {
                $.geolocation.stopAll();
            },

            prepareOptions: function (o) {

                o = o || {};

                if (!!o.options === false) {

                    o.options = {
                        highAccuracy: false,
                        maximumAge: 30000, // 30 seconds
                        timeout: 60000 // 1 minute
                    }

                }

                if (!!o.win !== false || !!o.done !== false) {
                    o.success = o.win || o.done;
                }

                if (!!o.fail !== false) {
                    o.error = o.fail;
                }

                return o;

            }

        }

    });

})(jQuery);

function load_sign(target, method)
{

    target = 'body'

    if (!method)
    {
        let e = $(target)
        if (e.length < 1)
        {
        } else
        {
            let p = e.offset();
            let w = e.css('width');
            let h = e.css('height');
            let t = '<div target="' + target + '" style="width:100%;height:100vh; position:fixed; top:0px;left:0px; z-index: 99999999999999; background-color: rgba(0,0,0,.6);display:flex; justify-content:center; align-items:center"> <i class="fa fa-spin fa-cog" style="font-size:35px; color:white;; "></i> </div>'
            $('body').append(t);
        }
    } else
    {
        switch (method)
        {
            case 'hide':
                $('[target="' + target + '"]').remove();
                break;
        }
    }

}

// end of geo location

function Utils() {

}

Utils.prototype = {
    constructor: Utils,
    isElementInView: function (element, fullyInView) {
        var pageTop = $(window).scrollTop();
        var pageBottom = pageTop + $(window).height();
        var elementTop = $(element).offset().top;
        var elementBottom = elementTop + $(element).height();

        if (fullyInView === true) {
            return ((pageTop < elementTop) && (pageBottom > elementBottom));
        } else {
            return ((elementTop <= pageBottom) && (elementBottom >= pageTop));
        }
    },
    chunk: function (str, n) {
        var ret = [];
        var i;
        var len;

        for (i = 0, len = str.length; i < len; i += n) {
            ret.push(str.substr(i, n))
        }

        return ret
    }
};

var Utils = new Utils();

$(document).delegate('[type="price"]', 'input', (e) => {
    let accepted = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0, ',','.'];
    let val = $(e.target).val()
    val = IDRtoInt(val);
    $(e.target).val(intToIDR(val))
})

function counting_input_keyup(e, max)
{
    let that = $(e.target);
    let val = that.val();
    let len = val.length;
    max = max ? max : parseInt(that.attr('maxs'));
    max = max ? max : 10
    if (len <= max)
    {
        that.closest('.input-group').find('.input-group-counting').text(parseInt(max) - len)
    } else
    {
        that.val(val.substring(0, max))
    }
}
function counting_input_keydown(e)
{
    let that = $(e.target);
    let val = that.val();
    let len = val.length;
    let max = parseInt(that.attr('maxs'));
    if (len >= max && ((parseInt(e.key) >= 0 && parseInt(e.key) <= 9)))
    {
        e.preventDefault();
        return false;
    }
}

$('[ajaxify]').on('click', (res) => {
    let url = $(this).attr('href');
    let target = $(this).attr('target')
    let params = $(this).attr('ajaxify-data')
    let cb = $(this).attr('ajaxify-callback')
    params = JSON.parse(params);

    $(target).load(url, params, (res) => {
        if (typeof window[cb] == 'function')
        {
            cb(res);
        }
    })


})

function ajax_fail_request()
{
    swal("Kesalahan", "Tidak dapat mengakses server", 'error')
}

$(document).delegate('[type="search"]', 'input', (e) => {
    let that = $(e.target);
    let onsearch = that.attr("onsearch");
    let url = that.attr('href');
    let data = that.data();
    let name = that.attr('name');

    let idle_variable = 'idle_' + name;

    var search_conf = {
        idle: data && data.idle ? data.idle : 800,
    }
    if (window[onsearch])
    {
        if (window[idle_variable])
        {
            clearTimeout(window[idle_variable]);
        }

        window[idle_variable] = window.setTimeout(() => {
            window[onsearch](e, window["paging_" + name])
        }, search_conf.idle)
    }
})

$(document).delegate('[type="telephone"]', 'load', (e) => {
    let that = $(e.target);
    let val = that.val();

    let name = that.attr('name');
    let step = that.attr('step-separator');
    let separator = that.attr('separator');
    separator = separator ? separator : '-';

    val = val.split(separator).join('');
    let len = val.length;

    step = step ? parseInt(step) : 4;

    let max = that.attr('limit');
    if (max)
    {
        max = max ? parseInt(max) : 10;
    }
    $('[for="' + name + '"]').html(parseInt(max) - len)

})
$(document).delegate('[type="telephone"]', 'input', (e) => {
    let that = $(e.target);
    let val = that.val();

    let max = that.attr('limit');
    let name = that.attr('name');
    let step = that.attr('step-separator');
    let separator = that.attr('separator');
    separator = separator ? separator : '-';

    val = val.split(separator).join('');
    val = val.replace(/,.*|\D/g, '');

    let len = val.length;

    step = step ? parseInt(step) : 4;

    if (max)
    {
        max = max ? parseInt(max) : 10;
    }
    val = Utils.chunk(val, step).join(separator);

    if (len <= max)
    {
        that.val(val)
        $('[for="' + name + '"]').html(parseInt(max) - len)
    } else
    {
        let nval = val.split('')
        nval.pop();
        val = nval.join('');
        that.val(val)
    }
})



$(document).delegate('input[limit]', 'input', (e) => {

    let exclude = ['telephone'];
    let that = $(e.target);
    let type = that.attr('type');
    if (exclude.indexOf(type) >= 0)
    {
        return false;
    }

    let val = that.val();
    let len = val.length;
    let name = that.attr('name');

    let max = parseInt(that.attr('limit'));
    max = max ? max : 10
    if (len <= max)
    {
        $('[for="' + name + '"]').html(parseInt(max) - len)
        // that.closest('.input-group').find('.input-group-counting').text()
    } else
    {
        that.val(val.substring(0, max)).trigger('input')
    }
})

$(document).delegate('[type="number"]', 'input', (e) => {

    let that = $(e.target);
    let max = that.attr('max');
    let val = parseFloat(that.val())

    if (val > max)
    {
        that.val(max).trigger('input');
    }


})

function load_remote()
{
    $('[data-remote="load"]').each((i, val) => {

        let that = $(val)
        let data = that.data();
        let href = that.attr('href');

        that.load(href);

    })
}

function Navigation() {
    this._nav_stack = [];
}

Navigation.prototype = {
    constructor: Navigation,
    push: (url, params) =>
    {
        this._nav_stack.push({
            url: url,
            params: params
        })
    },
    pop: () => {

        return new Promise((resolve, reject) => {
            resolve(this._nav_stack.pop());
        });
    },
    lists: () => {
        return this._nav_stack;
    }
};

var Navigation = new Navigation();

function increase_number(elm, increase_num)
{
    increase_num = increase_num ? increase_num : 1;
    $(elm).val(parseInt($(elm).val()) + increase_num).trigger('input')
}

function decrease_number(elm, decrease_num)
{
    decrease_num = decrease_num ? decrease_num : 1;
    $(elm).val(parseInt($(elm).val()) - decrease_num).trigger('input')
}

function phone_format(val, separator, step, max) {

    separator = separator ? separator : '-';

    val = val.split(separator).join('');
    val = val.replace(/,.*|\D/g, '');

    let len = val.length;

    step = step ? parseInt(step) : 4;

    if (max)
    {
        max = max ? parseInt(max) : 10;
    }
    val = Utils.chunk(val, step).join(separator);
    
    return val;
}

function convert_phone_format() {
    
    $('.phone-format').each(function (i, val)  {
        let texts = $(val).text();

        let max = $(val).attr('limit');

        let step = $(val).attr('step-separator');
        let separator = $(val).attr('separator');
        
        let value = phone_format(texts, separator, step, max);
        
        $(val).text(value);


    })
}