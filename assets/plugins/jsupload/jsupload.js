/*
------------------------------------------
upload_file.js
------------------------------------------
Plugin javascript ini digunakan untuk kepentingan upload file.
pengguna tidak perlu membuat controller untuk upload / addEventListener untuk onchange file. semua sudah diampu oleh upload file.
-------------------------------------------
#HOW TO USE

- initialize
let file = new upload_file('.element', {options})

- trigger browser file
file.browse();

- watching input file changed
file.watch_change(function)

- get uploaded file
file.get_data()

- get file_reader
file.reader()
.done(function)
.fail(function)

- return file as Formdata // in case you want to send using other ajax
file.get_file_formdata()
also you can pass other object to add to formdata
file.get_file_formdata(object)

- send file 
file.send(ajaxoptions)
.done(function)
.fail(function)

- get error
file.get_error();
---------------------------------------

Dependency : 
	- Jquery


*/
class Upload_file
{
	constructor(element, options)
	{
		let def_opts =
		{
			minimum_size: 10000000000000000000000,
			allowed_files: [], // typea, typeb, typec, | ['typea', 'typeb', ... ] / ['image', ]
			not_allowed_files: null,
			name: 'file',
			url: undefined,
			resetOnChange: true
		}
		this.name = new Date().valueOf();
		this.element = element||'<input type="file" name="'+name+'" class="sr-only">'
		this.e = $(element);
		this.options = $.extend(def_opts, options)
		this.files = []
		this.errors = []
		this.detect_change()
	}
	_is_minimum_size(file)
	{
		return file.size > this.options.minimum_size;
	}
	_is_allowed_extention(ext)
	{
		let type = this.options.allowed_files
		if(typeof this.options.allowed_files == 'string')
		{
			type = this.options.allowed_files.replace(/\s/g, '').split(',');
		}

		if(type.length < 1)
		{
			return true;
		}
	
		return ( type.indexOf(ext) > -1 )? true : false;
	}
	_get_extention(filename)
	{
		return filename.split('.').pop();
	}

	browse()
	{
		$(this.e).trigger('click')
	}

	set_data(data)
	{
		this.files.push(data)
	}
	get_data()
	{
		return this.files;
	}
	reset_data()
	{
		this.files = [];
	}

	set_error(code, text)
	{
		let error = this._error_list(code)
		error.text = text||error.text
		this.errors.push(error)
	}

	get_error()
	{
		return this.errors;
	}

	reset_error()
	{
		this.errors = [];
	}

	detect_change()
	{
		let self = this;
		$(document).delegate(this.element, 'change', function(e){
			let files = self.e[0].files;
			$.each(files, function(i, v){
				let ext = self._get_extention(v.name);
				let is_allow = self._is_allowed_extention(ext);
				let is_exceeds = self._is_minimum_size(v.size);
				if(is_allow && !is_exceeds)
				{
					v.unique = v.lastModified;
					console.log(v);
					self.set_data(v)
				}else
				{
					if(is_exceeds)
					{
						self.set_error('500.2');
					}
					if(!is_allow)
					{
						self.set_error('500.1');
					}
				}
			})
			e.errors = self.get_error();
			e.files = self.get_data();
			e.unique = new Date().valueOf();

			e.latest_file = files;
			
			var event = jQuery.Event( "file.change" );
			event.errors = self.get_error();
			event.files = self.get_data();
			event.latest_file = files;

			$(this).trigger(event)

			if(typeof onchange == 'function')
			{
				onchange(e);
			}
		})
	}

	watch_change(onchange)
	{
		$(this.element).on('file.change', onchange)
	}

	delete(unique)
	{
		let self = this;
		let deff = $.Deferred();

		let data = self.get_data();
		let index = data.map(function(res){return res.unique}).indexOf(unique);

		if(index < 0)
		{
			console.error(unique+" not recognize")
			deff.reject(unique, index, data)
		}else
		{
			let lates_data = data[index];
			self.get_data().splice(index, 1);
			var event = jQuery.Event( "file.remove" );
			event.data = self.get_data();
			event.latest_unique_removed = unique;
			event.latest_data_removed = lates_data;

			$(this.element).trigger(event)
			deff.resolve(event)
		}

		return $.when(deff.promise())
	}
	reset()
	{
		this.errors = []
		this.files = []
	}
	reader(data, timeout)
	{
		timeout = timeout? timeout : 1000;
		var def = $.Deferred();
	    if (data) {
	        var reader = new FileReader();

	        reader.onload = function (e) {
	    		if(typeof success == 'function'){
	    			success(e)
	    		}
    			setTimeout(function() {
	    			def.resolve(e)
    			}, timeout);
	        }

	        def.pipe(function(res){
	        	return res.reader = reader;
	        })
	        reader.readAsDataURL(data);
	    }
	    return $.when( def.promise() );
	}

	get_file_formdata(other_data)
	{
		let fData 	= new FormData(),
			data 	= this.get_data();

		$.each(data, function(a,b){
			fData.append(a,b);
		})

		if(other_data)
		{
			$.each(other_data, function(a,b){
				b = typeof b == 'object'? JSON.stringify(b) : b;
				fData.append(a,b);
			})
		}
		return fData;
	}
	send(options)
	{
		let self = this;
		options = $.extend({
			url: null,
			upload_progress: function(){},
			download_progress: function(){},
		}, options);

		if(!options.url)return;

		var deff  = $.Deferred(),
			fData = new FormData();  
		var data = self.get_data();
		if( options.url && Object.keys(data).length > 0 )
		{
			$.each(data, function(a,b){

				fData.append(a,b);
				$.each(b, function(c,d){
				})
			})

		}
		else
		{
			console.error('no url defined! or no file uploaded. sent data without file!');
		}

		if(options.data)
		{
			$.each(options.data, function(a,b){
				b = typeof b == 'object'? JSON.stringify(b) : b;
				fData.append(a,b);
			})
		}

		$.ajax({
		    url 	: options.url,
		    data 	: fData,
		    cache 	: false,
		    contentType : false,
		    processData : false,
		    type 		: 'POST',
		    success 	: function(data){
		    	deff.resolve(data);
		    	self.reset();
		    },
		    error: function(data)
		    {
		    	deff.reject(data);
		    },
		    xhr: function() {
		        var xhr = new window.XMLHttpRequest();
		        xhr.upload.addEventListener("progress", function(evt) {
		            if (evt.lengthComputable) {
		                var percentComplete = (evt.loaded / evt.total)*100;
		                options.upload_progress(event, percentComplete)
		                //Do something with upload progress here
		            }
		       }, false);

		       xhr.addEventListener("progress", function(evt) {
		           if (evt.lengthComputable) {
		               var percentComplete = (evt.loaded / evt.total)*100;
		                options.download_progress(event, percentComplete)
		               //Do something with download progress
		           }
		       }, false);

		       return xhr;
		    },

		});

		return deff.promise();
	}

	_error_list(code)
	{
		let error =  [
			{
				code:'500.1',
				text: 'Maaf file tidak dapat diproses karena termasuk dalam file yang tidak diperbolehkan. silahkan pilih file yang lain!'
			},
			{
				code:'500.2',
				text: 'Maaf ukuran file yang anda pilih melebihi batas minimum. silahkan pilih file yang lain!'
			},
		]

		if(code)
		{
			let index = error.map(function(res){
				return res.code
			}).indexOf(code);
			return error[index];
		}
		return error
	}



}