;(function ($) { $.fn.datepicker.language['id'] = {
    days: ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'],
    daysShort: ['Min', 'Sen', 'Sel', 'Rabu', 'Kam', 'Jum', 'Sab'],
    daysMin: ['Min', 'Sen', 'Sel', 'Rab', 'Kam', 'Jum', 'Sab'],
    months: ['Januari','Februari','Maret','April','Mei','Juni', 'Juli','Agustus','September','Oktober','November','Desember'],
    monthsShort: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sep', 'Okt', 'Nov', 'Des'],
    today: 'Hari ini',
    clear: 'Hapus',
    dateFormat: 'mm/dd/yyyy',
    timeFormat: 'hh:ii aa',
    firstDay: 0,
    am:'am',
    pm: 'pm',
    config_ampm: (time)=>{
        let r = '';
        if(time >= 0)
        {
            r =  'Dini'
        }
        if(time >= 6)
        {
            r =  'Pagi'
        }
        if(time >= 12)
        {
            r =  'Siang'
        }
        if(time >= 16)
        {
            r =  'Sore'
        }
        if(time >= 21)
        {
            r =  'Malam'
        }
        return r;
    }
}; })(jQuery);