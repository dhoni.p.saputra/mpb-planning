// window.notif = new Remote({host: 'https://remotep2p.herokuapp.com/', apiKey: 'Folarium3838' });
window.notif = new Remote({host: 'http://localhost:3000', apiKey: 'Folarium3838' });
var _s = Cookies.get('_s');

window.notif.listen(_s+'.request_employee.notif', function(data){
	console.log(data)
	if(typeof dataInvitationPage == 'function' )
	{
		ring_notif();
		dataInvitationPage();
	}
});

window.notif.listen('admin.notif.ring', function(data){
	ring_notif();
	if(data.toast)
	{
		$.toast({
	        heading: data.title,
	        text: data.text,
	        position: 'bottom-right',
	        loaderBg: '#fec107',
	        icon: 'success',
	        hideAfter: 3500,
	        stack: 8
	    });
	}

	console.log(data)
	if(typeof data.callback == 'function')
	{
		data.callback()
	}
})
window.notif.listen('admin.invoice.new', function(data){
	ring_notif();
	$.toast({
        heading: "Pembayaran baru diterima",
        text: "Ditemukan pembayaran outlet baru.",
        position: 'bottom-right',
        loaderBg: '#fec107',
        icon: 'success',
        hideAfter: 3500,
        stack: 8
    });
    window.dtInvoices.ajax.reload();
});

