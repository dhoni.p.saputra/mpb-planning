window.notif = new Remote({host: 'http://localhost:3000', apiKey: 'Folarium3838' });
// window.notif = new Remote({host: 'https://folariumremote.herokuapp.com/', apiKey: 'instantFolar3030' });
var _s = Cookies.get('_s');
console.log(_s)

window.notif.listen(_s+'.request_employee.notif', function(data){
	console.log(data)
	if(typeof dataInvitationPage == 'function' )
	{
		ring_notif();
		dataInvitationPage();
	}
});

window.notif.listen(_s+'.notif.ring', function(data){
	ring_notif();
	console.log(data);
	if(data.toast)
	{
		$.toast({
	        heading: data.title,
	        text: data.text,
	        position: 'bottom-right',
	        loaderBg: '#fec107',
	        icon: 'success',
	        hideAfter: 3500,
	        stack: 8
	    });
	}
})
window.notif.listen(_s+'.request_employee.accepted', function(data){
	console.log(data)
	if(typeof dataInvitationPage == 'function' )
	{
		ring_notif();
		// dataInvitationPage();
	}
});

window.notif.listen('admin.outlet.device:new-request', function(data){
	console.log(data)
	if(data.outlet_id == _s)
	{

		ring_notif();
		$.toast({
	        heading: "Permintaan perangkat",
	        text: "Terdapat permintaan perangkat terhubung.",
	        position: 'bottom-right',
	        loaderBg: '#fec107',
	        icon: 'success',
	        hideAfter: 3500,
	        stack: 8
	    });
	    window.dtDevices.ajax.reload();
	}
});