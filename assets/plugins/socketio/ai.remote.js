"use strict";

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Remote = function () {
	function Remote(options) {
		_classCallCheck(this, Remote);

		var deff = {
			host: undefined,
			apiKey: undefined,
			id: undefined
		};

		this.options = Object.assign(deff, options);
		this.options.url = this.options.host + '?cluster=' + this.options.apiKey;
		this.socket = io(this.options.url);
	}

	_createClass(Remote, [{
		key: "listen",
		value: function listen(event, fn) {
			event = this.options.apiKey + "_" + event;
			this.socket.on(event, fn);
		}
	}, {
		key: "send",
		value: function send(event, target, data, fn) {
			var t = this;
			var data = {
				target: target,
				data: data,
				_props: {
					apiKey: t.options.apiKey
				}
			};
			this.socket.emit(event, data, fn);
		}
	}]);

	return Remote;
}();