function order_save()
{
	
}

function toggle_order_pay()
{
	$('#receipt-pay, #receipt-action').toggle();
	setTimeout(()=>{
		$('[name="sell_paid"]').focus();
	},500)

}

function checkout_order(e)
{
	if(e.keyCode == 13)
	{
		Order.pay();
	}
}

function count_rest(event, that)
{
	let c = Cookies.getJSON('order-counter');
	that = $(that)
	let grand_total = c.grand_total;
	let val = that.val();
	val = IDRtoInt(val);
	let rest = val > grand_total? val - grand_total : 0;
	let debt = val < grand_total? grand_total - val : 0;

	$('[name="sell_debt"]').val(debt).trigger('input')
	$('[name="sell_rest"]').val(rest).trigger('input')

}
function Order(){
	this._init()
}

Order.prototype = {
    constructor: Order,
	_init: ()=>{
		this._order = Cookies.getJSON('orders')?Cookies.getJSON('orders'):[];
		this._ppn = 10;
		this._discount = 0;
		this._parent_class = 'list-group-item--item-order';
	},
	_check_item:(prod_id)=>{
		return this._order.map((res)=>{
			return res.prod_id;
		}).indexOf(prod_id);
	},
	default_order_item: (item)=>{
		let data = {

			sell_dt_id: item.sell_dt_id?sell_dt_id:null,
			prod_id: item.prod_id,
			sell_id: item.sell_id?item.sell_id:null,
			sell_dt_date: moment().format('YYYY-MM-DD HH:mm:ss'),
			sell_dt_qty: item.qty,
			sell_dt_price: item.prod_price,
			sell_dt_grand: item.grand_total,
			sell_dt_disc: item.disc?item.disc:0,
			sell_dt_disc_nom: item.disc_nominal?item.disc_nominal:0,
			sell_dt_total: item.total,
			sell_dt_note: '',
			sell_dt_status: 1,
			sell_dt_cancel_note: '',
		}
		return data;
	},
	get: (index)=>
	{
		if(index)
		{
			return Order._index;
		}
		return this._order;
	},
	set: (orders)=>
	{
		this._order = orders;
		Cookies.set('orders', this._order)
		Order.count_total();

	},
	update_order_item: (index, column, value)=>
	{
		this._order[index][column] = value;
		Order.count_per_item(index)
		Order.set(this._order);		
		Cookies.set('orders', this._order)

	},
	add: (item)=>{
		let index = Order._check_item(item.prod_id);
		if(index > -1)
		{
			this._order[index].sell_dt_qty = parseInt(this._order[index].sell_dt_qty) + 1;
			this._order[index].sell_dt_total = this._order[index].sell_dt_qty * this._order[index].sell_dt_price;
			this._order[index].sell_dt_grand = this._order[index].sell_dt_total;

		}else
		{
			item.qty = 1;
			item.total = item.qty * item.prod_price;
			item.grand_total = item.total;
			item = $.extend(item, Order.default_order_item(item));
			this._order.push(item)
		}

		Cookies.set('orders', this._order)
		Order.count_total();
	},

	refresh_view: (elem)=>{
		$(document).trigger('refresh-order');
	},
	count_per_item: (index)=>
	{
		this._order[index].sell_dt_total = this._order[index]['sell_dt_qty'] * this._order[index]['sell_dt_price'];
		this._order[index].sell_dt_disc = this._order[index].sell_dt_disc? this._order[index].sell_dt_disc : 0
		this._order[index].sell_dt_disc_nom = percentToNominal(this._order[index].sell_dt_disc, this._order[index].sell_dt_total)
		this._order[index].sell_dt_grand = this._order[index]['sell_dt_total'] - this._order[index]['sell_dt_disc_nom'];

		return this._order[index];
	},
	count_total: ()=>{
		let c = Cookies.getJSON('order-counter');
		let data = this._order;
		let d = {subtotal: 0}
		$.each(data, (i, val)=>{
			d.subtotal = d.subtotal + parseInt(val.sell_dt_grand);
		})
		d.tax = this._ppn;
		d.discount = this._discount;
		d.tax_nominal =  parseInt(percentToNominal(d.tax, d.subtotal));
		d.discount_nominal = parseInt(percentToNominal(this._discount, d.subtotal));
		d.grand_total = (d.subtotal - d.discount_nominal) + d.tax_nominal;

		if(c)
		{
			d = $.extend(c,d);
		}
		Cookies.set('order-counter', d);
		Order.refresh_view();
		if(data.length > 0)
		{
			$('#receipt-action').show();
		}else
		{
			$('[name="member_id"]').val('').trigger('change')
			$('#receipt-action').hide();
		}

		$(document).trigger('order:count')
	},
	get_order_property: ()=>{
		return Cookies.getJSON('order-counter');
	},
	set_detail_order: (name, val)=>{
		let d = Cookies.getJSON('order-counter');
		d[name] = val;
		Cookies.set('order-counter', d);
	},
	reset: ()=>{
		Cookies.set('orders', []);
		Cookies.set('order-counter', {subtotal:0, tax_nominal:0, tax:0, discount_nominal: 0, grand_total: 0});
		this._order = [];
		$(document).trigger('refresh-order');
		$('[name="sell_debt"]').val('').trigger('input')
		$('[name="sell_rest"]').val('').trigger('input')
		$('[name="sell_paid"]').val('').trigger('input')
		$('[name="member_id"]').val('').trigger('change')
		$('#receipt-action').hide();

	},
	order_decrease: (that)=>
	{
		that = $(that)
		let p = that.closest('.'+this._parent_class)
		let p_data = p.data();
		if(p_data.sell_dt_qty > 1)
		{
			let index = Order._check_item(p_data.prod_id);
			Order.update_order_item(index, 'sell_dt_qty', p_data.sell_dt_qty - 1)
		}else
		{
			Order.order_remove(that)
		}
		// removeArray(this._order, index);
		// Order.set(this._order)

	},
	order_increase: (that)=>
	{
		that = $(that)
		let p = that.closest('.'+this._parent_class)
		let p_data = p.data();
			let index = Order._check_item(p_data.prod_id);
			Order.update_order_item(index, 'sell_dt_qty', parseInt(p_data.sell_dt_qty) + 1)
		// removeArray(this._order, index);
		// Order.set(this._order)

	},
	order_remove: (that)=>
	{
		that = $(that)
		let p = that.closest('.'+this._parent_class)
		let p_data = p.data();
		let index = Order._check_item(p_data.prod_id);
		if(p_data.sell_dt_id)
		{

			let prom = new Promise((resolve, reject)=>{
				swal({
					title: "Hapus produk",
					text: "Silahkan isikan alasan penghapusan produk?",
					showCancelButton:true,
					cancelButtonText: "Batal",
					confirmButtonText: "Hapus",
					closeOnConfirm: false,
					type: "input",
					inputPlaceholder: "Alasan hapus"

				},(res)=>{
					if(res)
					{
						swal.disableButtons();
						resolve(res);
					}else
					{
						reject();
					}
				})
			})

			prom.then((res)=>{

				let url = base_url('transaction/sell/order/delete');
				let data = {sell_dt_cancel_note: res, sell_dt_id: p_data.sell_dt_id}
				$.ajax({
					url:url,
					data:data,
					dataType: 'JSON',
					type: "POST"
				})
				.done((res)=>{
					switch (res.code) {
						case 200:
							swal.close()
							$.mdtoast("Pembatalan produk selesai", {position: 'left'})
							if(this._order.length == 1)
							{
								Order.reset();
							}else
							{	
								removeArray(this._order, index);
								Order.set(this._order)
							}

							break;
						default:
							swal(res)
							break;
					}
				})
				
			})

		}else {
			
			if(this._order.length == 1)
			{
				Order.reset();
			}else
			{	
				removeArray(this._order, index);
				Order.set(this._order)
			}
		}

	},
	pay: ()=>{

		let c = Cookies.getJSON('order-counter');
		let o = Cookies.getJSON('orders');

		c.sell_paid = IDRtoInt($('[name="sell_paid"]').val());
		c.sell_rest = IDRtoInt($('[name="sell_rest"]').val());
		c.sell_debt = IDRtoInt($('[name="sell_debt"]').val());

		if(o.length < 1)
		{
			swal("Kesalahan", "Silahkan pilih produk terlebih dahulu", 'error')
			return false;
		}

		let deff_debt = new Promise((resolve, reject)=>{

			if(c.grand_total > c.sell_paid)
			{
				swal({
					title: "Pembayaran kurang",
					text: "Apakah anda yakin ingin menyimpan pembayaran? pembayaran akan disimpan sebagai hutang customer!",
					type: "warning",
					showCancelButton: true,
					closeOnConfirm: false,
					cancelButtonText: 'Batal',
					confirmButtonText: 'Simpan Hutang',
				}, (res)=>{
					if(res)
					{
						if(c.sell_name)
						{
							resolve(res);
						}else
						{
							swal({
								title: "Konsumen belum diisikan",
								text: "Silahkan isikan nama konsumen",
								type: "warning",
								showCancelButton: true,
								cancelButtonText: 'Batal',
								confirmButtonText: 'Isikan konsumen',
							}, (res)=>{
								if(res)
								{
									$('[name="member_id"]').select2('open')
								}
							})
							return false;
						}
					}else
					{
						reject(res);
					}
				})
			}else
			{
				resolve()
			}

		})

		// start the promise
		deff_debt.then(()=>{

			swal({
				title: "Bayar order",
				text: "Bayar order ini?",
				type: "warning",
				showCancelButton: true,
				closeOnConfirm: false,
				cancelButtonText: 'Batal',
				confirmButtonText: 'Bayar',
			}, (res)=>{
				if(res)
				{
					swal.disableButtons();

					c.sell_status = 2;
					Cookies.set('order-counter', c);
					Order.save()
					.then((res)=>{
						swal.close();
						switch (res.code) {
							case 200:
								setTimeout(()=>{
									swal("Terima kasih", "Kembalian sejumlah Rp. "+intToIDR(res.dataSent.sell_rest), "success")
								},100)
								
								break;
							default:
								break;
						}
					})
					
				}
			})

		}) // end of promise 
	},
	save: ()=>{
		return new Promise((resolve, reject)=>{

			let c = Cookies.getJSON('order-counter');
			let o = Cookies.getJSON('orders');

			let url = base_url('transaction/sell/save')
			let data = {
				sell_date: moment().format('YYYY-MM-DD HH:mm:ss'),
				sell_grand: c.grand_total,
				sell_tax: c.tax,
				sell_tax_nom: c.tax_nominal,
				sell_disc: c.discount,
				sell_disc_nom: c.discount_nominal,
				sell_total: c.subtotal,
				sell_paid: c.sell_paid?c.sell_paid : 0,
				sell_debt: c.sell_debt? c.sell_debt : 0,
				sell_rest: c.sell_rest? c.sell_rest : 0,
				sell_change: c.sell_change? c.sell_change : 0,
				sell_status: c.sell_status? c.sell_status : 1,
				sell_cancel_note: '',
				sell_id: c.sell_id?c.sell_id:null,
				sell_name: c.sell_name?c.sell_name:'',
				member_id: c.member_id?c.member_id:0,
				orders: o
			}

			$.ajax({
				type: "POST",
				data: data,
				url: url,
				dataType: 'JSON'
			})
			.done((res)=>{
				res.dataSent = data
				resolve(res)
				switch (res.code) {
					case 200:
						$.mdtoast("Penyimpanan berhasil", {position: 'left'})
						Order.reset();
						$('#receipt-pay').hide();
						// $('#receipt-action').show();

						break;
					default:
						swal(res)
						break;
				}
			})
			.fail((res)=>{
				reject(res)
			})
		
		})
	},

}

function detect_using_member()
{
	
		let use_member = $('[name="use_member"]').prop('checked');
}
$(document).ready(()=>{
	$('#receipt-pay').hide();
	window.Order = new Order(); 
	Order.count_total();
		

	detect_using_member();

})

$(document).on('refresh-order', (e)=>{
	let data = Cookies.getJSON('orders');
	let c = Cookies.getJSON('order-counter');

	let target = $('#list-group-order');
	let template = `
		<div class="list-group-item list-group-item--item-order flat ">
							<div class="list-group-item--content">
								<div class="order-item order-item--action">
									<button class="btn btn-danger btn-sm btn-action-table" onclick="Order.order_remove(this)"><i class="fa fa-trash"></i></button>	
								</div>
								<div class="order-item order-item--content">
									<strong>::prod_name::</strong>
								</div>
								<div class="form-group order-item order-item--qty">
									<div class="input-group"> 
										<div class="input-group-btn">
											<button class="btn btn-primary btn-sm btn-action-table" onclick="Order.order_decrease(this)">-</button>
										</div>
										<input type="price" class="form-control input-sm" name="" value="::sell_dt_qty::">
										<div class="input-group-btn">
											<button class="btn btn-primary btn-sm btn-action-table" onclick="Order.order_increase(this)"">+</button>
										</div>
									</div>
								</div>
								<div class="order-item order-item--total text-right">
									<strong>Rp. ::sell_dt_grand::</strong>

								</div>
							</div>
						</div>
	`; 
	write_data({
		target: target,
		template: template,
		records: data,
		dataCustom: (data)=>{
			data.total = intToIDR(data.total)
			data.sell_dt_grand = intToIDR(data.sell_dt_grand)
			data.prod_price = intToIDR(data.prod_price)
			data.tax_nominal = intToIDR(data.tax_nominal)
			return data
		}
	})

	$('.order--subtotal').text(intToIDR(c.subtotal))
	$('.order--tax').text(c.tax)
	$('.order--tax-nominal').text(intToIDR(c.tax_nominal))
	$('.order--discount').text(c.discount)
	$('.order--discount-nominal').text(intToIDR(c.discount_nominal))
	$('.order--total').text(intToIDR(c.grand_total))
	if(c.member_id)
	{
		setTimeout(()=>{
			$('[name="member_id"]').val(c.member_id).trigger('change')
		}, 100)
	}else if(c.sell_name)
	{
		var option = new Option(c.sell_name, c.sell_name, true, true);
    	$('[name="member_id"]').append(option).trigger('change');
		$('[name="member_id"]').val(c.sell_name).trigger('change')
	}


})

$('[name="member_id"]').on('change', (res)=>{

	setTimeout(()=>{
		$('#barcode-input').focus()	
	}, 100)
})
