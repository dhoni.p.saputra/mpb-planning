function Filtering(options)
{
	let deff_options = {
		name: new Date().valueOf(),
		search_delay: 700,

		data_url: undefined,
		form_url: undefined,
		remove_url: undefined,
		update_url: undefined,
		paging_url: undefined,
		toggle_status_url: undefined,
		
		search_fields : undefined,
		filter_fields: undefined,

		search_element: undefined,
		filter_element: undefined,
		toggle_status_element: undefined,

		load_data_target: undefined,
		load_form_target: undefined,

		show_per_paging: 10,
	}
	this.options = Object.assign(deff_options, options);

	this.search = function(){
		if(window[this.options.name+'_idle_search'])
		{
			clearTimeout(window[this.options.name+'_idle_search']);
		}

		window[this.options.name+'_idle_search'] = window.setTimeout(()=>{
			this._load_data();
		}, this.options.search_delay)
	}
	this.load_data = function(page){
		this._load_data(page);
	}
	this.load_form = function(page){
		this._load_form(page);
	}	

	this.paging = function(event, per_page)
	{
		event.preventDefault();
		let page = $(event.target).attr('data-ci-pagination-page');
		console.log(page)
		page = (page - 1) * per_page;
		this._load_data( page );
	}

	this.set_option = function(opt, value){
		this.options[opt] = value;
	}

	this.toggle_status = function(data)
	{
		return this._toggle_status(data)
	}

	this.remove = function(data)
	{
		return this._remove(data)
	}
	this.that = this;
}	

Filtering.prototype = {
	that: {},
	_toggle_status: function(data)
	{
		let toggle_status_url = base_url(this.options.toggle_status_url)
		return this._process('post', toggle_status_url, data)
	},
	_remove: function(data)
	{
		let remove_url = base_url(this.options.remove_url)
		return this._process('post', remove_url, data)
	},
	_filter: function(){
		let params = {}
		if(this.options.search_fields)
		{
			let value = $(this.options.search_element).val();

			let search_fields = this.options.search_fields;
			let like = search_fields[0];
			let or_like = this.options.search_fields;
			params.like = [[like, value]]
			if(this.options.search_fields.length > 0)
			{
				params.or_like = []
				$.each(or_like, (i, val)=>{
					if(i > 0)
					{
						params.or_like.push([val, value])
					}
				});
			}
		}


		return params
	},
	_load_data: function(page){
		this.options.page = page? page : '';
		let data_url = base_url(this.options.data_url+'/'+this.options.page);
		return new Promise((clb, err)=>{
			let params = this.that._filter();
			$(this.options.load_data_target).load(data_url, params, (res)=>{
					clb(res)
			})
		})
	},
	_load_form: function(page){
		page = page?page:'';

		let form_url = base_url(this.options.form_url)+'/'+page
		return new Promise((clb, err)=>{
			let params = this.that._filter();
			$(this.options.load_form_target).load(form_url, params, (res)=>{
					clb(res)
			})
		})
	},

	_process: (type, url, data)=>
	{
		return $.ajax({
			url: url,
			data: data,
			type: type,
			dataType: "JSON"
		})
	}
}