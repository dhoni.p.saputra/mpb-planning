/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 5.7.19 : Database - mpb_dummy
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mpb_dummy` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mpb_dummy`;

/*Table structure for table `md_checklist` */

DROP TABLE IF EXISTS `md_checklist`;

CREATE TABLE `md_checklist` (
  `checklist_id` int(11) NOT NULL AUTO_INCREMENT,
  `checklist_done` tinyint(1) DEFAULT '0',
  `tahapan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`checklist_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_checklist` */

/*Table structure for table `md_client` */

DROP TABLE IF EXISTS `md_client`;

CREATE TABLE `md_client` (
  `client_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_name` varchar(50) DEFAULT NULL,
  `client_type` int(11) DEFAULT NULL,
  `client_data` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_client` */

/*Table structure for table `md_detail_invoice` */

DROP TABLE IF EXISTS `md_detail_invoice`;

CREATE TABLE `md_detail_invoice` (
  `invoice_id` int(11) NOT NULL,
  `UR_id` int(11) NOT NULL,
  `duration` bigint(20) DEFAULT NULL,
  `bill_rate` double DEFAULT NULL,
  PRIMARY KEY (`invoice_id`,`UR_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_detail_invoice` */

/*Table structure for table `md_doc_picture` */

DROP TABLE IF EXISTS `md_doc_picture`;

CREATE TABLE `md_doc_picture` (
  `picture_id` int(11) NOT NULL AUTO_INCREMENT,
  `picture_url` text,
  `checklist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`picture_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_doc_picture` */

/*Table structure for table `md_doc_project` */

DROP TABLE IF EXISTS `md_doc_project`;

CREATE TABLE `md_doc_project` (
  `PDOC_id` int(11) NOT NULL AUTO_INCREMENT,
  `PDOC_url` text,
  `PDOC_content` text,
  `checklist_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`PDOC_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_doc_project` */

/*Table structure for table `md_invoice` */

DROP TABLE IF EXISTS `md_invoice`;

CREATE TABLE `md_invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_due` datetime DEFAULT NULL,
  `invoice_send` datetime DEFAULT NULL,
  `users_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_invoice` */

/*Table structure for table `md_project` */

DROP TABLE IF EXISTS `md_project`;

CREATE TABLE `md_project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` varchar(200) DEFAULT NULL,
  `project_start` datetime DEFAULT NULL,
  `project_end` datetime DEFAULT NULL,
  `project_budget` double DEFAULT NULL,
  `project_type` enum('StandAlone','AJOL','AJOM','NAJO') DEFAULT NULL,
  PRIMARY KEY (`project_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `md_project` */

insert  into `md_project`(`project_id`,`project_name`,`project_start`,`project_end`,`project_budget`,`project_type`) values 
(1,'Project A',NULL,NULL,NULL,NULL),
(2,'Project B',NULL,NULL,NULL,NULL);

/*Table structure for table `md_resource` */

DROP TABLE IF EXISTS `md_resource`;

CREATE TABLE `md_resource` (
  `resource_id` int(11) NOT NULL AUTO_INCREMENT,
  `resource_name` varchar(100) DEFAULT NULL,
  `resource_cost` double DEFAULT NULL,
  `resource_type` enum('human','FFE') DEFAULT NULL,
  `resource_billrate` double DEFAULT NULL,
  PRIMARY KEY (`resource_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `md_resource` */

insert  into `md_resource`(`resource_id`,`resource_name`,`resource_cost`,`resource_type`,`resource_billrate`) values 
(1,'Resource A',13000,'human',3000000),
(2,'Resource B',NULL,'FFE',30000),
(3,'Resource D',NULL,'FFE',543534),
(4,'Resource C',NULL,'FFE',232352);

/*Table structure for table `md_tahapan` */

DROP TABLE IF EXISTS `md_tahapan`;

CREATE TABLE `md_tahapan` (
  `tahapan_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahapan_host` int(11) DEFAULT NULL,
  `tahapan_name` varchar(100) DEFAULT NULL,
  `tahapan_description` text,
  `tahapan_milestone` tinyint(1) DEFAULT '0',
  `tahapan_budget` double DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `tahapan_registered` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`tahapan_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `md_tahapan` */

insert  into `md_tahapan`(`tahapan_id`,`tahapan_host`,`tahapan_name`,`tahapan_description`,`tahapan_milestone`,`tahapan_budget`,`project_id`,`tahapan_registered`) values 
(1,NULL,'Supervisi A',' ',0,NULL,2,'2018-07-31 09:31:45');

/*Table structure for table `md_template_tahapan` */

DROP TABLE IF EXISTS `md_template_tahapan`;

CREATE TABLE `md_template_tahapan` (
  `tahapan_temp_id` int(11) NOT NULL AUTO_INCREMENT,
  `tahapan_temp_child_id` int(11) DEFAULT NULL,
  `tahapan_temp_name` varchar(50) DEFAULT NULL,
  `tahapan_temp_description` text,
  PRIMARY KEY (`tahapan_temp_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `md_template_tahapan` */

/*Table structure for table `mg_used_resource` */

DROP TABLE IF EXISTS `mg_used_resource`;

CREATE TABLE `mg_used_resource` (
  `UR_id` int(11) NOT NULL AUTO_INCREMENT,
  `UR_start` datetime DEFAULT NULL,
  `UR_end` datetime DEFAULT NULL,
  `resource_id` int(11) DEFAULT NULL,
  `tahapan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`UR_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `mg_used_resource` */

insert  into `mg_used_resource`(`UR_id`,`UR_start`,`UR_end`,`resource_id`,`tahapan_id`) values 
(1,'2018-07-01 00:00:00','2018-07-10 00:00:00',1,1);

/*Table structure for table `tahapan_timeline` */

DROP TABLE IF EXISTS `tahapan_timeline`;

CREATE TABLE `tahapan_timeline` (
  `tahapan_start` datetime DEFAULT NULL,
  `tahapan_end` datetime DEFAULT NULL,
  `tahapan_portion` double DEFAULT NULL,
  `tahapan_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `tahapan_timeline` */

insert  into `tahapan_timeline`(`tahapan_start`,`tahapan_end`,`tahapan_portion`,`tahapan_id`) values 
('2018-07-01 00:00:00','2018-07-10 00:00:00',NULL,1);

/*Table structure for table `temp_doc` */

DROP TABLE IF EXISTS `temp_doc`;

CREATE TABLE `temp_doc` (
  `TDOC_id` int(11) NOT NULL AUTO_INCREMENT,
  `TDOC_name` varchar(100) DEFAULT NULL,
  `TDOC_content` text,
  PRIMARY KEY (`TDOC_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `temp_doc` */

/*Table structure for table `temp_tahapan` */

DROP TABLE IF EXISTS `temp_tahapan`;

CREATE TABLE `temp_tahapan` (
  `tahapan_id` int(11) DEFAULT NULL,
  `tahapan_child_id` int(11) DEFAULT NULL,
  `tahapan_name` varchar(100) DEFAULT NULL,
  `tahapan_description` text
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `temp_tahapan` */

/*Table structure for table `url_image_cke` */

DROP TABLE IF EXISTS `url_image_cke`;

CREATE TABLE `url_image_cke` (
  `UIC_id` int(11) NOT NULL AUTO_INCREMENT,
  `UIC_url` text,
  `PDOC_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`UIC_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

/*Data for the table `url_image_cke` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
