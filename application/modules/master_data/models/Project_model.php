<?php 

/**
 * 
 */
class Project_model extends CI_Model
{
	
    public $table_name = "md_project";
    public $table_use = "mg_used_resource";

	function __construct()
	{
		# code...
		parent::__construct();
         $this->fields = array(
            'project_id'          => $this->table_name.'.project_id',
            'project_name'         => $this->table_name.'.project_name',
            
        );
	}


    public function get($request = FALSE)
    {
        $default['fields'] = 'project_id,project_name';
        $this->grab->permitted_request_type('GET', $request);

        // set default and masking database fields
        $this->grab->masking($this->fields);

        // set default fields when from original source not available
        $this->grab->default_fields($default);

        $this->grab->process(array(
            
        ));
        $this->grab->database->from($this->table_name);

        $q = $this->grab->compile();
        if($this->grab->is_error())
        {
            $result['error'] = $q;
            $result['is_error'] = TRUE;
            return $result;
        }
        // echo $this->db->last_query();
        return $q;
    }


    public function remove($where)
    {
        $this->grab->masking($this->fields);
        $where = $this->grab->convert_object_array_to_original($where);
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->delete($this->table_name, $where);
        return true;
    }

    public function update($data, $where)
    {
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $where = $this->grab->convert_object_array_to_original($where);
        $data = $this->grab->get_table_data($this->table_name, $data);    
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->where($where);
        $this->db->update($this->table_name, $data);
        return $this->db;
    }

     public function insert($data)
    {
        $temp = $data;
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $data = $this->grab->get_table_data($this->table_name, $data);    

        $this->db->insert($this->table_name, $data);
        $insert = $this->db;
        return $insert;
    }


}