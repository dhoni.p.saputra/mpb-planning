<?php 

/**
 * 
 */
class Resource_model extends CI_Model
{
	
    public $table_name = "md_resource";
    public $table_use = "mg_used_resource";

	function __construct()
	{
		# code...
		parent::__construct();
         $this->fields = array(
            'resource_id'          => $this->table_name.'.resource_id',
            'resource_name'         => $this->table_name.'.resource_name',
            'resource_cost'          => $this->table_name.'.resource_cost',
            'resource_type'    => $this->table_name.'.resource_type',
            'resource_billrate'    => $this->table_name.'.resource_billrate',
        );

         $this->fields_use = array(
            'UR_id'          => $this->table_use.'.UR_id',
            'UR_start'         => $this->table_use.'.UR_start',
            'UR_end'          => $this->table_use.'.UR_end',
            'resource_id'    => $this->table_use.'.resource_id',
            'tahapan_id'    => $this->table_use.'.tahapan_id',
        );
	}


    public function get($request = FALSE)
    {
        $default['fields'] = 'resource_id,resource_name,resource_cost,resource_type,resource_billrate';
        $this->grab->permitted_request_type('GET', $request);

        // set default and masking database fields
        $this->grab->masking($this->fields);

        // set default fields when from original source not available
        $this->grab->default_fields($default);

        $this->grab->process(array(
            
        ));
        $this->grab->database->from($this->table_name);

        $q = $this->grab->compile();
        if($this->grab->is_error())
        {
            $result['error'] = $q;
            $result['is_error'] = TRUE;
            return $result;
        }
        // echo $this->db->last_query();
        return $q;
    }


    public function remove($where)
    {
        $this->grab->masking($this->fields);
        $where = $this->grab->convert_object_array_to_original($where);
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->delete($this->table_name, $where);
        return true;
    }

    public function update($data, $where)
    {
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $where = $this->grab->convert_object_array_to_original($where);
        $data = $this->grab->get_table_data($this->table_name, $data);    
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->where($where);
        $this->db->update($this->table_name, $data);
        return $this->db;
    }

     public function insert($data)
    {
        $temp = $data;
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $data = $this->grab->get_table_data($this->table_name, $data);    

        $this->db->insert($this->table_name, $data);
        $insert = $this->db;
        return $insert;
    }



    public function get_r_use($request = FALSE)
    {
        $default['fields'] = 'UR_id,UR_start,UR_end,resource_id,tahapan_id';
        $this->grab->permitted_request_type('GET', $request);

        // set default and masking database fields
        $this->grab->masking($this->fields_use);

        // set default fields when from original source not available
        $this->grab->default_fields($default);

        $this->grab->process(array(
            
        ));
        $this->grab->database->from($this->table_use);

        $q = $this->grab->compile();
        if($this->grab->is_error())
        {
            $result['error'] = $q;
            $result['is_error'] = TRUE;
            return $result;
        }
        // echo $this->db->last_query();
        return $q;
    }


    public function remove_r_use($where)
    {
        $this->grab->masking($this->fields_use);
        $where = $this->grab->convert_object_array_to_original($where);
        $where = $this->grab->get_table_data($this->table_use, $where);

        $this->db->delete($this->table_use, $where);
        return true;
    }

    public function update_r_use($data, $where)
    {
        $this->grab->masking($this->fields_use);
        $data = $this->grab->convert_object_array_to_original($data);
        $where = $this->grab->convert_object_array_to_original($where);
        $data = $this->grab->get_table_data($this->table_use, $data);    
        $where = $this->grab->get_table_data($this->table_use, $where);

        $this->db->where($where);
        $this->db->update($this->table_use, $data);
        return $this->db;
    }

     public function insert_r_use($data)
    {
        $temp = $data;
        $this->grab->masking($this->fields_use);
        $data = $this->grab->convert_object_array_to_original($data);
        $data = $this->grab->get_table_data($this->table_use, $data);    

        $this->db->insert($this->table_use, $data);
        $insert = $this->db;
        return $insert;
    }



}