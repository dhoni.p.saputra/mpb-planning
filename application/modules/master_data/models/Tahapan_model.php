<?php 

/**
 * 
 */
class Tahapan_model extends CI_Model
{
	
    public $table_name = "md_tahapan";
    public $table_tahapan_inline = "tahapan_timeline";

	function __construct()
	{

		# code...
		parent::__construct();
        $this->load->model('resource_model');

        $this->fields = array(
            'tahapan_id'          => $this->table_name.'.tahapan_id',
            'tahapan_host'         => $this->table_name.'.tahapan_host',
            'tahapan_name'          => $this->table_name.'.tahapan_name',
            'tahapan_description'    => $this->table_name.'.tahapan_description',
            'tahapan_milestone'    => $this->table_name.'.tahapan_milestone',
            'tahapan_budget'    => $this->table_name.'.tahapan_budget',
            'project_id'    => $this->table_name.'.project_id',
            'tahapan_registered'    => $this->table_name.'.tahapan_registered',
            'dependencies' => '(select count(tahapan_id) from md_tahapan m where m.tahapan_host = md_tahapan.tahapan_id)',
            'min_date'      => '(SELECT MIN(tahapan_start) FROM tahapan_timeline where tahapan_timeline.tahapan_id = md_tahapan.tahapan_id )',
            'max_date'      => '(SELECT MAX(tahapan_end) FROM tahapan_timeline where tahapan_timeline.tahapan_id = md_tahapan.tahapan_id )',
            'timeline'      => ''
        );

        $this->tahapan_timeline = array(
            'tahapan_start'          => $this->table_tahapan_inline.'.tahapan_start',
            'tahapan_end'         => $this->table_tahapan_inline.'.tahapan_end',
            'tahapan_portion'          => $this->table_tahapan_inline.'.tahapan_portion',
            'tahapan_id'    => $this->table_tahapan_inline.'.tahapan_id',
        );
	}


    public function get($request = FALSE)
    {
        $default['fields'] = 'tahapan_id,tahapan_host,tahapan_name,tahapan_description,tahapan_milestone,tahapan_budget,project_id,tahapan_registered,timeline,dependencies';
        $this->grab->permitted_request_type('GET', $request);

        // set default and masking database fields
        $this->grab->masking($this->fields);

        // set default fields when from original source not available
        $this->grab->default_fields($default);

        $this->grab->process(array(
            'timeline' => function($params, $data)
            {
                return $this->get_tahapan_timeline(array(
                    'where' => array(
                        'tahapan_id' => $data['tahapan_id']
                    )
                ))['data']->result_array();
            },
            'resource' => function($params, $data)
            {
                return $this->resource_model->get_r_use(array(
                    'where' => array(
                        'tahapan_id' => $data['tahapan_id']
                    )
                ))['data']->result_array();
            }
        ));
        $this->grab->database->from($this->table_name);

        $q = $this->grab->compile();
        if($this->grab->is_error())
        {
            $result['error'] = $q;
            $result['is_error'] = TRUE;
            return $result;
        }
        // echo $this->db->last_query();
        return $q;
    }

    public function remove($where)
    {
        $this->grab->masking($this->fields);
        $where = $this->grab->convert_object_array_to_original($where);
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->delete($this->table_name, $where);
        return true;
    }

    public function update($data, $where)
    {
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $where = $this->grab->convert_object_array_to_original($where);
        $data = $this->grab->get_table_data($this->table_name, $data);    
        $where = $this->grab->get_table_data($this->table_name, $where);

        $this->db->where($where);
        $this->db->update($this->table_name, $data);
        return $this->db;
    }

     public function insert($data)
    {
        $temp = $data;
        $this->grab->masking($this->fields);
        $data = $this->grab->convert_object_array_to_original($data);
        $data = $this->grab->get_table_data($this->table_name, $data);    

        $this->db->insert($this->table_name, $data);
        $insert = $this->db;
        return $insert;
    }



    public function get_tahapan_timeline($request = FALSE)
    {
        $default['fields'] = 'tahapan_start,tahapan_end,tahapan_portion,tahapan_id';
        $this->grab->permitted_request_type('GET', $request);

        // set default and masking database fields
        $this->grab->masking($this->tahapan_timeline);

        // set default fields when from original source not available
        $this->grab->default_fields($default);

        $this->grab->process(array(
            
        ));
        $this->grab->database->from($this->table_tahapan_inline);

        $q = $this->grab->compile();
        if($this->grab->is_error())
        {
            $result['error'] = $q;
            $result['is_error'] = TRUE;
            return $result;
        }
        // echo $this->db->last_query();
        return $q;
    }

    public function insert_timeline($data)
    {
        $temp = $data;
        $this->grab->masking($this->tahapan_timeline);
        $data = $this->grab->convert_object_array_to_original($data);
        $data = $this->grab->get_table_data($this->table_tahapan_inline, $data);    

        $this->db->insert($this->table_tahapan_inline, $data);
        $insert = $this->db;
        return $insert;
    }

    public function remove_timeline($where)
    {
        $this->grab->masking($this->tahapan_timeline);
        $where = $this->grab->convert_object_array_to_original($where);
        $where = $this->grab->get_table_data($this->table_tahapan_inline, $where);

        $this->db->delete($this->table_tahapan_inline, $where);
        return true;
    }

}