<div class="panel panel-default">
	<div class="panel-heading">Form Project</div>
	<div class="panel-body">
		<div class="form-group">
			<label>Nama project</label>
			<input type="text" name="project_name" class="form-control" required="" placeholder="Nama proyek">
		</div>
		<div class="form-group">
			<label>Estimasi tanggal project</label>
			<input type="text" name="project_date" id="project_date" class="form-control" required="" placeholder="Tanggal proyek">
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function(){
		$('#project_date').datepicker({
			range:true,
			language: 'en',
			multipleDatesSeparator: ' - '
		});
	})
</script>