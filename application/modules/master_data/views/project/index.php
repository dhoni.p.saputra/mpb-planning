<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/air_datepicker').'/css/datepicker.min.css' ?>">
<script type="text/javascript" src="<?php echo base_url('assets/plugins/air_datepicker').'/js/datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/air_datepicker').'/js/i18n/datepicker.en.js' ?>"></script>

<form method="post" action="<?php echo base_url('project/save') ?>">
	<div class="form-group">
		<input type="text" class="form-group" name="project_name" required="" placeholder="Nama project">
		<button class=""> Buat project </button>
	</div>
</form>

<div class="row">
	<div class="col-md-4">
		<div class="row">
			<div class="col-md-6">
				<button class="btn btn-block btn-primary flat" onclick="load_form_project()">
					Buat Proyek
				</button>
			</div>
			<div class="col-md-6">
				<button class="btn btn-block btn-info flat" onclick="load_form_template()">
					Buat Template tahapan
				</button>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div id="form-section" class="mt-10">
					
				</div>
			</div >
		</div>	
	</div>
	<div class="col-md-8">
		
		<table class="table table-bordered table-hovered table-striped">
			<thead>
				<tr>
					<th>No.</th>
					<th>Nama</th>
					<th>Aksi</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($project as $key => $value): ?>
					<tr>
						<td><?php echo $key + 1 ?></td>
						<td><?php echo $value['project_name'] ?></td>
						<td> <a class="btn btn-primary" href="<?php echo base_url('project/'.$value['project_id']) ?>"> Buka </a> </td>
					</tr>
				<?php endforeach ?>
			</tbody>
		</table>
		 
	</div>
</div>


<script type="text/javascript">
	
	function load_form_project()
	{
		let url = base_url('project/form')
		$('#form-section').load(url)
	}

	function load_form_template()
	{
		let url = base_url('tahapan/template/form')
		$('#form-section').load(url)
	}

	load_form_project();
</script>