<div class="mt-20">
	
	<div class="list-group">
		<div class="list-group-item">
			<label>Tahapan</label>
			<div class=""> <?php echo $data['tahapan_name'] ?> </div>
		</div>
		<div class="list-group-item">
			<label>Panjang tahapan</label>
			<div class=""> <?php echo date('d F Y', strtotime($data['min_date'])) ?> - <?php echo date('d F Y', strtotime($data['max_date']) ) ?> </div>
		</div>
	</div>

	<div>
		<button class="btn btn-warning mt-20 btn-block flat" onclick="load_form(<?php echo $data['tahapan_id'] ?>)"> <i class="fa fa-edit"></i> </button>
	</div>
</div>