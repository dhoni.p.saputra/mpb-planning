<div class="row mt-20">
	<div class="col-md-5">
		<div class="form-group" id="filter-timeline">
			<?php  
				$mlm = new MLMenu($tahapan, null, null, 'tahapan_id', 'tahapan_host', 'tahapan_name');
				echo $mlm->write('select');
			?>
		</div>
	</div>
	<div class="col-md-5">
		
			<div class="form-group">
				<button class="btn btn-warning" onclick="load_timeline_data(0)">Lihat semua</button>
			</div>


	</div>
	<div class="col-md-12">
			<div class="a"></div>
	</div>
</div>

<script type="text/javascript">
	load_timeline_data(0)

	$('select').addClass('form-control')
	$('#filter-timeline select').on('change', function(e){
		let val = $(this).val();
		let index = window.$tahapan.map(function(res){
			return res.tahapan_id
		}).indexOf(val)

		load_timeline_data(val)
	})
</script>