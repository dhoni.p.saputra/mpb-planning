<form class="" onsubmit="submit_tahapan(event)" autocomplete="off">
	<?php if (isset($data)): ?>
		<input type="hidden" name="tahapan_id" value="<?php echo $data['tahapan_id']; ?>" required>
	<?php endif ?>
	<div class="form-group">
		<label>Nama tahapan</label>
		<input class="form-control" type="text" name="tahapan_name" autocomplete="off" value="<?php echo isset($data)? $data['tahapan_name'] : ''; ?>" required>
	</div>
	<div class="form-group">
		<label>Deskripsi tahapan</label>
		<textarea class="form-control" name="tahapan_description" autocomplete="off"><?php echo isset($data)? $data['tahapan_description'] : ''; ?> </textarea>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-5">
			<div class="form-group">
				<label>Resource</label>
				<select class="form-control" id="resource-form-select">
					<option> --Pilihan data-- </option>
					<?php foreach ($resource as $key => $value): ?>
						<option data-id="<?php echo $value['resource_id'] ?>" value="<?php echo $value['resource_id'] ?>"> <?php echo $value['resource_name'] ?> </option>
					<?php endforeach ?>
				</select>
			</div>
			
		</div>

		<div class="col-md-5">
			<div class="form-group">
				<label>Tgl. Guna </label>
				<input type="text" name="" id="date_resource" class="form-control" autocomplete="off">
			</div>
		</div>

		<div class="col-md-2">
			<button class="btn btn-primary btn-action-table mt-25" type="button" onclick="resource_onchange(event)"> <i class="fa fa-plus"></i> </button>
		</div>
		

		<div class="col-md-12">
			<div id="resource_append">
				<?php if (isset($resource_data)): ?>
					<?php foreach ($resource_data as $key => $value): ?>
						<div class="mb-10 row" data-id="<?php echo $value['resource_id'] ?>">
							<input type="hidden" name="resource_id[]" class="form-control" value="<?php echo $value['resource_id'] ?>"> 
							<input type="hidden" name="resource_date_start[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($value['UR_start'])) ?>">
							<input type="hidden" name="resource_date_end[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($value['UR_end'])) ?>">

							<div class="col-md-5">
								<?php echo $value['resource_name'] ?>
							</div>
							<div class="col-md-5">
								<?php echo date('Y-m-d', strtotime($value['UR_start'])) ?> - <?php echo date('Y-m-d', strtotime($value['UR_end'])) ?>
							</div>
							<div class="col-md-2">
								<button onclick="remove_resource(this)" data-id="`+val+`"> <i class="fa fa-trash"></i> </button>
							</div>
						</div>
					<?php endforeach ?>
				<?php endif ?>
			</div>
			
		</div>
	</div>
	<hr>
	<div class="form-group" id="tahapan-form-select">
		<label>Induk tahapan</label><br>
		<label class="">
			<input type="checkbox" id="top_level" name="top_level" <?php echo isset($data)? (empty($data['tahapan_host'])? 'checked' : '') : 'checked' ?>> Paling atas
		</label>
		<?php 
			$arr = array('tahapan_id' => 0, 'tahapan_host' => 0, 'tahapan_name' => 'Paling atas');
			array_unshift($tahapan, $arr);
			$mlm = new MLMenu($tahapan, null, null, 'tahapan_id', 'tahapan_host', 'tahapan_name');
			echo $mlm->write('select');
		?>
	</div>
	<hr>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label>Tanggal tahapan</label>
				<input type="text" name="" id="date_tahapan" class="form-control" autocomplete="off">
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<button type="button" class="btn btn-info btn-block mt-25" onclick="tambah_tanggal_tahapan()">Tambahkan </button>
				
			</div>
			
		</div>
	</div>
	<div class="">
		<div id="tahapan_timeline_append">
			<?php if (isset($data['timeline'])): ?>
				<?php foreach ($data['timeline'] as $key => $value): ?>
					
					<div class="mb-10 row">
						<div class="col-md-5">
							<input type="date" name="date_start[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($value['tahapan_start'])) ?>"> 
						</div>
						<div class="col-md-5">
							<input type="date" name="date_end[]" class="form-control" value="<?php echo date('Y-m-d', strtotime($value['tahapan_end'])) ?>">
						</div>
						<div class="col-md-2">
							<button onclick="remove_calendar(this)"> <i class="fa fa-trash"></i> </button>
						</div>
					</div>

				<?php endforeach ?>
			<?php endif ?>
		</div>
	</div>
	<hr>
	
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<button class="btn btn-danger btn-block" type="button" onclick="load_form()"> Batal </button>
			</div>
		</div>
		<div class="col-md-6">
			<div class="form-group">
				<button class="btn btn-primary btn-block" type="submit"> <?php echo isset($data)? 'Update' : 'Simpan'; ?> </button>
			</div>
		</div>
	</div>
</form>

<script type="text/javascript">
	
	$('select').addClass('form-control')

	$('#date_tahapan, #date_resource').datepicker({
		range:true,
		language: 'en',
		multipleDatesSeparator: ' - '
	});

	$('#tahapan-form-select select').val(null)
	<?php if (isset($data)): ?>	
		$('#tahapan-form-select select').val(<?php echo $data['tahapan_host'] ?>)
	<?php endif ?>

	window.$resource = <?php echo json_encode($resource) ?>;

	window.$resource_use = [];

	$('#tahapan-form-select #tahapan_host').on('change', function(res){
		$('#top_level').prop('checked',false)
	})

	function resource_onchange(e)
	{
		let that = $('#resource-form-select');
		let val = that.val();
		
		let date = $('#date_resource').val();
		date = date.split(' - ');

		date[0] = moment(date[0]).format('YYYY-MM-DD')
		date[1] = moment(date[1]).format('YYYY-MM-DD')

		let resource_index = window.$resource.map(function(res){
			return res.resource_id;
		}).indexOf(val);

		let exist = window.$resource_use.indexOf(val);

		that.find('option[data-id="'+val+'"]').hide();

		let data = $resource[resource_index];

		let temp = `<div class="mb-10 row" data-id="`+val+`">
			<input type="hidden" name="resource_id[]" class="form-control" value="`+val+`"> 
			<input type="hidden" name="resource_date_start[]" class="form-control" value="`+date[0]+`">
			<input type="hidden" name="resource_date_end[]" class="form-control" value="`+date[1]+`">

			<div class="col-md-5">
				 `+data.resource_name+`
			</div>
			<div class="col-md-5">
				 `+date[0]+` - `+date[1]+`
				
			</div>
			<div class="col-md-2">
				<button onclick="remove_resource(this)" data-id="`+val+`"> <i class="fa fa-trash"></i> </button>
			</div>
		</div>`;
		$('#resource_append').append(temp)

	}

	function remove_resource(that)
	{
		that = $(that);
		let select = $('#resource-form-select');
		let id = that.data('id');

		let exist = window.$resource_use.indexOf(id);

		removeArray($resource_use, exist);
		
		select.find('option[data-id="'+id+'"]').show();


		that.closest('.row').remove();
	}

	function tambah_tanggal_tahapan()
	{
		let date = $('#date_tahapan').val();
		date = date.split(' - ');

		date[0] = moment(date[0]).format('YYYY-MM-DD')
		date[1] = moment(date[1]).format('YYYY-MM-DD')
		let temp = `<div class="mb-10 row">
			<div class="col-md-5">
				<input type="date" name="date_start[]" class="form-control" value="`+date[0]+`"> 
			</div>
			<div class="col-md-5">
				<input type="date" name="date_end[]" class="form-control" value="`+date[1]+`">
			</div>
			<div class="col-md-2">
				<button onclick="remove_calendar(this)"> <i class="fa fa-trash"></i> </button>
			</div>
		</div>`;
		$('#tahapan_timeline_append').append(temp)
	}

	function remove_calendar(that)
	{
		that = $(that);

		that.closest('.row').remove();
	}
	function submit_tahapan(e)
	{
		e.preventDefault();
		let that = $(e.target)

		let data = that.serializeArray();
		console.log(data)
		$.ajax({
			data: data,
			url: base_url('tahapan/method/save'),
			type: "POST",
			dataType: "JSON"
		})
		.done((res)=>{
			console.log(res)
			switch(res.code)
			{
				case 200:
					$.mdtoast("Data berhasil disimpan", {position: "right"})
					load_form();
					load_timeline(0)

					break;
			}
		})


	}
</script>