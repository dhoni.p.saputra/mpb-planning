<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/air_datepicker').'/css/datepicker.min.css' ?>">
<script type="text/javascript" src="<?php echo base_url('assets/plugins/air_datepicker').'/js/datepicker.min.js' ?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/plugins/air_datepicker').'/js/i18n/datepicker.en.js' ?>"></script>

<style type="text/css">
	.sidebar-right
	{
		width: 260px;
		height: 100vh;
		position: fixed;
		background-color: #ECF0F1;
		top: 0px;
		overflow-y: auto;
		right: 10px;
	}
	.sidebar-right #sidebar
	{
		height: 80%;

	}
</style>

<div class="row">
	<div class="col-md-3" id="form">
		
	</div>
	<div class="col-md-9">
		<div id="timeline_index"></div>
	</div>	
</div>

<div class="sidebar-right sr-only">
	<div class="row">
		<div class="col-md-12">
			<button class="btn btn-primary flat" onclick="toggle_sidebar_tahapan()"> <i class="fa fa-times"></i> </button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="sidebar">
			
		</div>
	</div>
</div>
<hr>


<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">
	
	window.$tahapan = <?php echo json_encode($tahapan); ?>;
	function toggle_sidebar_tahapan(status)
	{
		switch(status)
		{
			case 'open' : 
				$('.sidebar-right').removeClass('sr-only')
				break;
			default : 
			case 'close' : 
				$('.sidebar-right').addClass('sr-only')
				break;
		}
	}

	function load_sidebar(id)
	{
		id = id?id:'';
		let o = base_url('tahapan/sidebar/'+id);
		$('#sidebar').load(o, function(){
    		toggle_sidebar_tahapan('open');
		})

	}
	function load_form(id)
	{
		id = id?id:'';
		let url_form = base_url('tahapan/form/'+id);
		$('#form').load(url_form)
	}	

	function load_timeline_data(val)
	{
		val = val?val:0;
		let url = '<?php echo base_url('tahapan/timeline/data') ?>'
		$('.a').load(url, {tahapan_id: val})
	}

	function load_timeline(val)
	{
		val = val?val:0;
		let url = '<?php echo base_url('tahapan/timeline') ?>'
		$('#timeline_index').load(url)
	}



	$(function(){
		load_form();
		load_timeline();
	
		
	})
</script>