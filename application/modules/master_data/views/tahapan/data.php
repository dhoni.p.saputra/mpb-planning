

<script type="text/javascript">
window.$jira = <?php echo json_encode($jira) ?>;
window.$tahapan = <?php echo json_encode($tahapan) ?>;

window.$timeline = [];
$.each($jira, function(i, val){
	// window.$timeline[i] = [];
	window.$timeline.push([val[0], val[0], new Date(val[1]), new Date(val[2]) ])
	$.each(val, function(ind, value){
	})
})
  google.charts.load('current', {'packages':['timeline']});
  google.charts.setOnLoadCallback(drawChart);
  function drawChart() {
    var container = document.getElementById('timeline');
    var chart = new google.visualization.Timeline(container);
    var dataTable = new google.visualization.DataTable();

    google.visualization.events.addListener(chart, 'select', function(res){
    	let selection = chart.getSelection();
    	let name = dataTable.getValue(selection[0].row, 0);

    	let index = window.$tahapan.map(function(res){
    		return res.tahapan_name;
    	}).indexOf(name);

    	load_sidebar($tahapan[index]['tahapan_id']);
    });

    /*data.addColumn('string', 'Task ID');
    data.addColumn('string', 'Task Name');
    data.addColumn('date', 'Start Date');
    data.addColumn('date', 'End Date');
    data.addColumn('number', 'Duration');
    data.addColumn('number', 'Percent Complete');
    data.addColumn('string', 'Dependencies');*/

    dataTable.addColumn({ type: 'string', id: 'Term' });
    dataTable.addColumn({ type: 'string', id: 'Name' });
    dataTable.addColumn({ type: 'date', id: 'Start' });
    dataTable.addColumn({ type: 'date', id: 'End' });

    dataTable.addRows($timeline);

    chart.draw(dataTable, {
    	timeline: {
    		showRowLabels: true
    	}
    });
  }
</script>

	<div id="timeline" style="min-height: 80vh;"></div>
