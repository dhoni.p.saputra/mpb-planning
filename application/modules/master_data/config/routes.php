<?php

$route['project'] = 'master_data/project/index';

$route['project/save'] = 'master_data/project/save';
$route['project/form'] = 'master_data/project/form';

$route['project/(:num)'] = 'master_data/project/open_project/$1';

$route['tahapan'] = 'master_data/tahapan/index';
$route['tahapan/form'] = 'master_data/tahapan/tahapan_form';
$route['tahapan/sidebar/(:any)'] = 'master_data/tahapan/tahapan_sidebar/$1';
$route['tahapan/form/(:any)'] = 'master_data/tahapan/tahapan_form/$1';
$route['tahapan/method/save'] = 'master_data/tahapan/tahapan_save';
$route['tahapan/timeline'] = 'master_data/tahapan/timeline';
$route['tahapan/timeline/data'] = 'master_data/tahapan/timeline_data';

$route['tahapan/template/form'] = 'master_data/template_tahapan/form';
