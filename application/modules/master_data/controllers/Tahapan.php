<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tahapan extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('tahapan_model', 'resource_model'));
        $this->controller = "tahapan";
	}
	public function index()
	{

	 	$data['title'] = "Tahapan";
        
        $data['nav_side'] = array(
            // 'content' => 'template/nav/nav_default'
        );

        $data['content'] = $this->controller."/index";

        $data['pass'] = $data;

		$data['tahapan'] = $this->tahapan_model->get(array(
			'where' => array(
				'project_id' => $_SESSION['id']
			)
		))['data']->result_array();


        $this->load->view("folarium/index", $data);
	}

	private function min_max_date($data)
	{
		$min = 0;
		$max = 0;
		foreach ($data as $key => $value) {
			$min = $min < strtotime($value['tahapan_start']) ? $value['tahapan_start'] : $min;
			$max = $min < strtotime($value['tahapan_end']) ? $value['tahapan_end'] : $max;
		}

		return array('min' => $min, 'max' => $max);
	}

	public function tahapan_sidebar($id)
	{

	 	$data['title'] = "Tahapan";
        
        $data['nav_side'] = array(
            // 'content' => 'template/nav/nav_default'
        );

        $data['content'] = $this->controller."/tahapan_sidebar";

        $data['pass'] = $data;

		$p['where']['tahapan_id'] = $id;
		$this->grab->add_to_field('min_date,max_date');

		$data['data'] 		= $this->tahapan_model->get($p)['data']->row_array();

		$data['timeline'] = $this->tahapan_childs($id);

		$data['min_max'] = $this->min_max_date($data['timeline']);

        $this->load->view($data['content'], $data);
	}

	public function tahapan_form($id=null)
	{
		$p = array();
		if(!empty($id))
		{
			$p['where']['tahapan_id'] = $id;
			$data['data'] 		= $this->tahapan_model->get($p)['data']->row_array();

			$this->grab->force_add_to_field('resource_name');
			$data['resource_data'] = $this->resource_model->get_r_use(array(
				'where' => array(
					'tahapan_id' => $id,
				),
				'join' => array(
					array('md_resource', 'md_resource.resource_id', 'mg_used_resource.resource_id')
				)
			))['data']->result_array();
		}

		$data['resource'] 	= $this->resource_model->get()['data']->result_array();

		
		$data['tahapan'] 	= $this->tahapan_model->get(array(
		'where' => array(
			'project_id' => $_SESSION['id']
		)) )['data']->result_array();
		$this->load->view('tahapan/form', $data);

	}

	public function tahapan_save()
	{

		$post = $this->input->post(null,true);
		
		$post['project_id'] = $_SESSION['id'];
		if(isset($post['tahapan_id']))
		{
			$this->tahapan_model->update($post, array('tahapan_id' => $post['tahapan_id']) );

			$tahapan_id = $post['tahapan_id'];
			$this->tahapan_model->remove_timeline(array('tahapan_id' => $post['tahapan_id']));

			$this->resource_model->remove_r_use(array('tahapan_id' => $post['tahapan_id']));

		}else
		{
			$post['tahapan_host'] = isset($post['top_level'])? null : $post['tahapan_host'];
			$this->tahapan_model->insert($post);
			$tahapan_id = $this->db->insert_id();
		}

		foreach ($post['date_start'] as $key => $value) {
			$d_ins = array(
				'tahapan_start' => $value,
				'tahapan_end' 	=> $post['date_end'][$key],
				'tahapan_id' 	=> $tahapan_id
			);
			$this->tahapan_model->insert_timeline($d_ins);
		}

		if(isset($post['resource_id']))
		{
			foreach ($post['resource_id'] as $key => $value) {
				$r_ins = array(
					'UR_start' 		=> 	$post['resource_date_start'][$key],
					'UR_end' 	=> $post['resource_date_end'][$key],
					'resource_id' 	=> $value,
					'tahapan_id' 	=> $tahapan_id
				);
				$this->resource_model->insert_r_use($r_ins);
			}
		}

		if ($this->db->affected_rows() > 0) {
            $response['code']    = 200;
            $response['message'] = 'Data successfully inserted';
        } else {
            $response['code']    = 500;
            $response['message'] = 'Error on saving the data.';
        }

        echo json_encode($response);
	}

	public function timeline()
	{
		$post = $this->input->post(null,true);


		$data['tahapan'] = $this->tahapan_model->get(array(
			'where' => array(
				'project_id' => $_SESSION['id']
			)
		))['data']->result_array();

		


		$this->load->view('tahapan/timeline_index', $data);

	}

	public function timeline_data()
	{
		$post = $this->input->post(null,true);

		if($post['tahapan_id'] > 0)
		{
			$post['where']['tahapan_id'] = $post['tahapan_id'];
		}else
		{
			$post['where']['tahapan_id'] = null;
		}
		$post['where']['project_id'] = $_SESSION['id'];



		$data['tahapan'] = $this->tahapan_childs($post['where']['tahapan_id']);

		
		$this->grab->force_add_to_field('resource_name');
		$data['resource'] = $this->resource_model->get_r_use(array(
			'where' => array(
				'tahapan_id' => $post['tahapan_id']
			),
			'join' => array(
				array('md_resource', 'md_resource.resource_id', 'mg_used_resource.resource_id')
			)
		))['data']->result_array();

		$data['jira'] = array();


		if(count($data['tahapan']) > 0)
		{
			$mlm = new MLMenu($data['tahapan'], null, null, 'tahapan_id', 'tahapan_host', 'tahapan_name');
			$data['tahapan'] = $mlm->data;
		}

		foreach ($data['tahapan'] as $k => $v) {
			$data['jira'][] = array($v['tahapan_name'], date('Y-m-d', strtotime($v['tahapan_start'])), date('Y-m-d', strtotime($v['tahapan_end'])), $v['tahapan_id'] );
	
		}


		$this->load->view('tahapan/data', $data);

	}

	public function tahapan_childs($host=null)
	{
		$this->node = array();
		$this->deep = array();
		$this->tahapan_childs_filtering($host);
		return $this->node;
	}

	private function tahapan_childs_filtering($host, $deep = 0)
	{
		$post['fields'] = 'tahapan_id,tahapan_host,tahapan_name,tahapan_description,tahapan_milestone,tahapan_budget,project_id,tahapan_registered,dependencies'; 
		$post['join'][] = array('tahapan_timeline', 'md_tahapan.tahapan_id', 'tahapan_timeline.tahapan_id', 'left');
		$post['where']['tahapan_host'] = $host;
		$post['where']['project_id'] = $_SESSION['id'];

		$this->grab->force_add_to_field('tahapan_start,tahapan_end');
		$tahapan = $this->tahapan_model->get($post)['data']->result_array();

		$records = array();
		foreach ($tahapan as $key => $value) {
			if(isset($this->deep[$value['tahapan_host']]))
			{
				$value['deep'] = $this->deep[$value['tahapan_host']] + 1;
				$this->deep[$value['tahapan_id']] = $value['deep'];
			}else
			{
				$value['deep'] = 0;
				$this->deep[$value['tahapan_id']] = $value['deep'];
			}
			
			$this->node[] = $value;

			if($value['dependencies'] > 0)
			{
				if(!in_array($value['tahapan_id'], $records))
				{
					$this->tahapan_childs_filtering($value['tahapan_id'], $deep + 1);
					$records[] = $value['tahapan_id'];
				}

			}else
			{
				$deep = 0;
			}
		}

	}
}
