<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Project extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model(array('project_model', 'resource_model'));
        $this->controller = "tahapan";
	}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{

		$data['title'] = "Project";
        
        $data['nav_side'] = array(
            // 'content' => 'template/nav/nav_default'
        );

        $data['content'] = 'project/index';

        $data['pass'] = $data;

		$data['project']  = $this->db->get('md_project')->result_array();

        $this->load->view("folarium/index", $data);
	}

	public function form()
	{

        $this->load->view("project/form");
	}

	public function save()
	{
		$post = $this->input->post(null, true);
		$this->db->insert('md_project', $post);
		redirect('project');
	}

	public function open_project($id)
	{
		$_SESSION['id'] = $id;
		redirect('tahapan');

	}
}
