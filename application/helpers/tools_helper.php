<?php 

function getRandomString($length = 8, $usingChar = FALSE) {
    $characters = '0123456789';
    if($usingChar)
    {
    	$characters .= 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    }
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

function weekOfMonth($date, $rollover = 'monday') {
    /*// Get the first day of the month.
    $date = is_string($date)? strtotime($date):$date;
    $firstOfMonth = strtotime(date("Y-m-01", $date));
    // Apply above formula.
    return intval(date("W", $date)) - intval(date("W", $firstOfMonth)) + 1;*/

    $firstOfMonth = strtolower(date('l',strtotime(date("Y-m-01", strtotime($date)))) );
    $cut = substr($date, 0, 8);
    $daylen = 86400;

    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;

    $weeks = 1;

    for ($i = 1; $i <= $elapsed; $i++)
    {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));
        // echo $firstOfMonth.' '.$day.' '.date('d',$daytimestamp);
        var_dump($day == strtolower($rollover));
        var_dump($firstOfMonth != $day);
        var_dump(date('d',$daytimestamp) != 1);
        echo '---'."\n";
        if($day == strtolower($rollover) && (date('d',$daytimestamp) != 1) )   $weeks ++;
    }

    return $weeks;
}
function remove_all_whitespace($string='')
{
    return preg_replace('/\s+/', '', $string);
}

function get_bracket_content($text = '')
{
    preg_match('#\((.*?)\)#', $text, $match);
    return $match; 
}

function getContentBetween($startDelimiter, $endDelimiter, $str) {
  $contents = array();
  $startDelimiterLength = strlen($startDelimiter);
  $endDelimiterLength = strlen($endDelimiter);
  $startFrom = $contentStart = $contentEnd = 0;
  while (false !== ($contentStart = strpos($str, $startDelimiter, $startFrom))) {
    $contentStart += $startDelimiterLength;
    $contentEnd = strpos($str, $endDelimiter, $contentStart);
    if (false === $contentEnd) {
      break;
    }
    $contents[] = substr($str, $contentStart, $contentEnd - $contentStart);
    $startFrom = $contentEnd + $endDelimiterLength;
  }

  return $contents;
}

function replace_between($needle_start, $needle_end, $str, $replacement) {
    $pos = strpos($str, $needle_start);
    $start = $pos === false ? 0 : $pos + strlen($needle_start);

    $pos = strpos($str, $needle_end, $start);
    $end = $pos === false ? strlen($str) : $pos;

    return substr_replace($str, $replacement, $start, $end - $start);
}

function advance_explode($str, $separator=",", $leftbracket="(", $rightbracket=")", $quote="'", $ignore_escaped_quotes=true ) {

    $buffer = '';
    $stack = array();
    $depth = 0;
    $betweenquotes = false;
    $len = strlen($str);
    for ($i=0; $i<$len; $i++) {
        $previouschar = @$char;
        $char = $str[$i];
        switch ($char) {
            case $separator:
            if (!$betweenquotes) {
                if (!$depth) {
                    if ($buffer !== '') {
                        $stack[] = $buffer;
                        $buffer = '';
                    }
                    continue 2;
                }
            }
            break;
            case $quote:
            if ($ignore_escaped_quotes) {
                if ($previouschar!="\\") {
                    $betweenquotes = !$betweenquotes;
                }
            } else {
                $betweenquotes = !$betweenquotes;
            }
            break;
            case $leftbracket:
            if (!$betweenquotes) {
                $depth++;
            }
            break;
            case $rightbracket:
            if (!$betweenquotes) {
                if ($depth) {
                    $depth--;
                } else {
                    $stack[] = $buffer.$char;
                    $buffer = '';
                    continue 2;
                }
            }
            break;
        }
        $buffer .= $char;
    }
    if ($buffer !== '') {
        $stack[] = $buffer;
    }

    return $stack;
}

function create_dir($filepath)
{
    if(!is_array($filepath))
    {
        $filepath = explode('/', $filepath);
    }
    $dirpath = array_shift($filepath);
    $dir = $dirpath;
    foreach ($filepath as $key => $value) {
        $dir .= '/'.$value;
        if(!is_dir($dir))
        {
            mkdir($dir,0777);
            $dir.='/';
        }
    }
    $dir = rtrim($dir,'/').'/';
    return $dir;
}
function create_file($new_dir)
{
    $filepath   = explode('/', $new_dir);
    $filename   = array_pop($filepath);
    $dir        = create_dir($filepath);
    $file       = $dir.$filename;
    file_put_contents($file, '');
    return $file;
}