<?php
define('DB_HOST','localhost'); /* use p:HOSTNAME (presistent connection) !! not recommended !! */
define('DB_USER','root');
define('DB_PASS','');
define('DB_DATA','basisdata');
define('DB_PORT','3306');
@session_start();
class dataAkses
{
    protected $mysqli;
	function __construct()		
	{
        // $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATA, DB_PORT);  
        $this->mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_DATA);  
    }
    function flat_arr($array,&$result){
	  foreach($array as $v){
	    if(is_array($v)){
	      self::flat_arr($v,$result);
	    }else{
	      $result[]=$v;
	    }
	  }
	  return $result;
	}
	function SQL(){
		$args=func_get_args();
		if (isset($args[0])) {
			$stmt = mysqli_stmt_init($this->mysqli);
			if ($stmt->prepare($args[0])) {
				if(isset($args[1])){
					$x = substr_count($args[0],'?');
					$y = array_fill(0,$x,"");
					if (strnatcmp(phpversion(),'5.3') >= 0) //Reference is required for PHP 5.3+
					{
					    foreach($y as $key=>$value) {
					       @$param[$key] = &$y[$key];
					    }
					}else{
						$param=$y;
					}
					call_user_func_array("mysqli_stmt_bind_param",array_merge(array($stmt),array($args[1]),$param));
					unset($x,$y,$refs,$i);
					$x=array_slice($args,2);
					//flattening the array
					self::flat_arr($x,$flat_arr);
					foreach ($flat_arr as $value) {
			            $y[]=$value;							
			        }
			        $i = 0;
			        foreach($param as $key=>$value) {
		               $param[$key] = $y[$i];
		               $i++;
		            }
				}
				$stmt->execute();
				$x = $stmt->field_count;
				// return$x;
				if($x>0){
					$stmt->store_result();
					$meta = $stmt->result_metadata();
				 	while($field = $meta->fetch_field()){
				 		$result[$field->name] = &$refs[$field->name];
				 	}
					call_user_func_array("mysqli_stmt_bind_result",array_merge(array($stmt),$result));
					$i=0;
					while ($stmt->fetch()) {
						foreach ($result as $key => $value) {
							$data[$i][$key]=$value;
						}
						$i++;
				 	}
					$stmt->free_result();
					$stmt->close();
					return $data=!empty($data)?$data:-1;
				}				
	            $stmt->close();
			}
		}
	}
	function beginTransaction()
	{
		$this->mysqli->begin_transaction();
	}
	function commitOff()
    {
        $this->mysqli->autocommit(FALSE);
    }
    function commitOn()
    {
        if (!$this->mysqli->commit()){
            return 0;
        }else{
            return 1;
        };
    }
    function insertID()
    {
        return $this->mysqli->insert_id;
    }
    function affectedRows()
    {
        return $this->mysqli->affected_rows;
    }
    function __destruct()
    {
        $this->mysqli->close();
    }
/* data hashing & verification */
	function hashCrypt($hash)
	{
	    $crypt[0]=substr($hash, 0,16);
	    $crypt[1]=substr($hash, 16);
	    return $crypt;
	}
	function hashSome($pass)
	{
	    $cost=8;
	    $options = [
	    'cost' => $cost,
	    'salt' => mcrypt_create_iv(22, MCRYPT_DEV_URANDOM),
	    ];
	    $cost=10;
	    $options = [
	        'cost' => $cost,
	        'salt' => password_hash($pass, PASSWORD_BCRYPT,$options),
	    ];
	    return $this->hashCrypt(password_hash($pass, PASSWORD_BCRYPT,$options));
	}
	function verifly($pass,$hash)
	{
		switch ($pass) {
			case empty($pass):
				return -1;
			default:
				$veri = $this->hashSome($pass);
				if(password_verify($pass,$veri[0].$hash)==1)
					return $veri[1];
		}	
		return -1;
	}
}
class Auth extends dataAkses
{
	/*
	function signUp();
	{
		 use $this->SQL instead 
	}
	*/
	function signIn($user,$pass,$type,$gotoPage='')
	{
		unset($_SESSION['auth']);
		$view = $this->SQL("SELECT * FROM $type WHERE $type"."_email=? or $type"."_phone=?","ss",$user,$user);
		if ($view!==-1) {
			if( ($veri = $this->verifly($pass,array_pop($view[0]))) !== -1){
				$_SESSION['auth']['veri']=$veri;
				$_SESSION['auth']['as']=$type;
				$this->setAuthSession($view[0]);
				$this->SQL("UPDATE $type SET $type"."_token=? WHERE $type"."_email=? or $type"."_phone=?","ss",$veri,$user,$user);
			}
		}
		$this->authorized($gotoPage);		
	}
	private function veriThis()
	{
		return $this->SQL("SELECT $this->type"."_token FROM $this->type WHERE $this->type"."_email=? or $this->type"."_phone=?","ss",$_SESSION['auth']['email'],$_SESSION['auth']['phone']);
	}
	function authorized($gotoPage=''){
		if(isset($_SESSION['auth']['as'])){
			$this->type=$_SESSION['auth']['as'];
			$veri=$this->veriThis();
			if (!empty($_SESSION['auth']['veri'])==!empty($veri[0][$this->type.'_token']) && strlen($_SESSION['auth']['veri'])==44){
				return 1;
			}
		}
		$this->logout();
		$this->gotoPage($gotoPage);
			
	}
	function relogin($gotoPage){
		if(isset($_SESSION['auth']['as'])){
			$this->type=$_SESSION['auth']['as'];
			$veri=$this->veriThis();
			if (!empty($_SESSION['auth']['veri'])==!empty($veri[0][$this->type.'_token']) && strlen($_SESSION['auth']['veri'])==44){
				$this->gotoPage($gotoPage);
			}
		}
	}
	function gotoPage($gotoPage='')
	{
		$gotoPage=empty($gotoPage)?'login.php':$gotoPage;
		header("Location: $gotoPage");
	}
	function setAuthSession($a)
	{
		array_pop($a);
		foreach ($a as $key => $value) {
			$key=explode('_',$key);
			$_SESSION['auth'][$key[1]]=$value;
		}
	}
	function logout($gotoPage='')
	{
		if (isset($_SESSION['auth'])) {
			unset($_SESSION['auth']);
		}
		$this->gotoPage($gotoPage);
	}
	function getProfile()
	{
		if(isset($_SESSION['auth'])){
			$record=array_slice($_SESSION['auth'],2);
			foreach ($record as $key => $value) {
				$profile[$key]=$value;
			}
			return $profile;
		}
	}
}

class arraySession
{
	function beforeInsert($index,$id,$session)
	{
		foreach ($session as $key => $value) {
			if ($key[$index]==$id) {
				return 1;
			}
		}
	}
	function replaceElementValue(&$session,$index,$replace)
	{
		$session[$index]=$replace;
	}
	function deleteElementIndex(&$session,$index)
	{
		array_splice($session, $index,1);
	}
	/* 
	*  function addToCart() 
	*  {
	*	 # use inline scripting (resource efficiency and good benchmark solution).
	*  } 
	*
	*  function cartRendering()
	*  {
	*    # use inline scripting (resource efficiency, css customizable and good benchmark solution).
	*  }
	*
	*/
}

class Pagination
/*
$katalog=new dataAkses();
function writeKatalog($data)
{
	$echo='';
	foreach ($data as $f) {
		$echo.="<div>produk id = $f[id]</div><div>nama = $f[nama]</div><div>harga = $f[harga]</div>";
	}
	return $echo;
}
$fun=new pagination($katalog,'produk');
$set['offset']="5,10,20";
$set['sort'][]="nama,nama A-Z,nama Z-A";
$set['sort'][]="kategori,kategori 0-N,kategori N-0";
$set['search'][]="nama,Nama Produk";
$set['search'][]="kategori,No Kategori";
$set['data']="SELECT produk_ID as id, produk_name as nama, produk_jual as harga, kategori_ID as kategori FROM Produk";
$set['write']['func']="writeKatalog";

if(isset($_POST['href'])){
	$set['write']['urlParam']=$_POST['href'];
	echo $fun->rewrite($set);
}else{
	$set['write']['urlParam']=$_SERVER['QUERY_STRING'];
	echo $fun->setup($set);
}
*/
{
	
	function __construct($connector,$ref,$offset=10,$pages=5)
	{
		/* html variable */
		$this->htmlOffset='';
		$this->htmlSort='';
		$this->htmlSearch='';
		$this->write='no data available';
		$this->pageLink='';
		/* content vaiable */
		$this->connector=$connector;
		$this->ref=$ref;
		$this->offset=$offset;
		$this->sort='';
		$this->search='';
		$this->page=1;
		$this->pages=$pages;
	}
	function setup($param)
	{
		$this->param=$param;
		if (isset($param['data'])){$this->data($param['data']);}else{return "error data nowhere found";}
		if (isset($param['write'])){$this->write($param['write']);}
		return <<<XXX
	
<div class="cplusco-paging-head" data-paging="head">
$this->htmlOffset
$this->htmlSort
$this->htmlSearch
<span class="cplusco-paging-reset">
<button type="button" class="cplusco-paging-reset" data-paging="reset">reset</button>
</span>
</div>
<div class="cplusco-paging-body" data-paging="body">
<div class="cplusco-paging-content" data-paging="content">$this->write</div>
<div class="cplusco-paging-pagelink" data-paging="pagelink">$this->pageLink</div>
</div>
</div>
XXX
;
	}
	function rewrite($param)
	{
		$this->param=$param;
		foreach ($param as $key => $value) {
			if ($key=='write' || $key=='data') {
				$this->$key($value);
			}
		}
		return '<div class="cplusco-paging-content" data-paging="content">'.$this->write.'</div><div class="cplusco-paging-pagelink" data-paging="pagelink">'.$this->pageLink.'</div>';
	}
	function setOffsetDefault($array)
	{
		$offset=isset($this->getoffset)?$this->getoffset:$this->offset;
		rsort($array);
		$defaultOffset=0;
		foreach ($array as $value) {

			if ($offset <= $value) {
				$defaultOffset=$value;
			}
		}
		$this->offset=$defaultOffset<1?$array[0]:$defaultOffset;
		return $this->offset;
	}
	function offset()
	{
		if (!isset($this->param['offset'])) {
			$param="10,20,50";
		}else{
			$param=$this->param['offset'];
		}
		$param=explode(',', $param);
		$this->setOffsetDefault($param);
		$this->htmlOffset='<span class="cplusco-paging-offset"><label for="offset">#item</label><select data-paging="option" name="cplusco-paging-offset">';
		foreach ($param as $key => $value) {
			$offset=(isset($this->getoffset) && $value==$this->getoffset)?'selected':($value==$this->offset?'selected':'');
			$this->htmlOffset.="<option value='$value' $offset>$value</option>";
		}
		$this->htmlOffset.="</select></span>";
	}
	function sort($param)
	{
		$this->htmlSort='<span class="cplusco-paging-sort"><label for="sort">Sort by</label><select data-paging="option" name="cplusco-paging-sort"><option></option>';
		foreach ($param as $key => $value) {
			$v=explode(',', $value);
			$sort[0]=isset($this->getsort[0]) && $this->getsort[0]==$v[0] && (!isset($this->getsort[1])||empty($this->getsort[1])||$this->getsort[1]=="ASC")?'selected':'';
			$sort[1]=isset($this->getsort[0]) && isset($this->getsort[1]) && $this->getsort[0]==$v[0] && $this->getsort[1]=="DESC"?'selected':'';
			if(!empty($v[1])){
				$this->htmlSort.="<option data-sort='$v[0]' value='ASC' $sort[0]>$v[1]</option>";
			}
			if(!empty($v[2]) && isset($v[2])){
				$this->htmlSort.="<option data-sort='$v[0]' value='DESC' $sort[1]>$v[2]</option>";
			}
		}
		$this->htmlSort.="</select></span>";
	}
	function search($param)
	{
		$search[0]=isset($this->getsearch[1])?$this->getsearch[1]:'';
		$this->htmlSearch='<span class="cplusco-paging-search"><label for="search">Search</label><input type="search" data-paging="option" name="cplusco-paging-search" value='.$search[0].'><select data-paging="option" name="cplusco-paging-by">';
		foreach ($param as $key => $value) {
			$value=explode(',', $value);
			$search[1]=isset($this->getsearch[0]) && $this->getsearch[0]==$value[0]?'selected':'';
			$this->htmlSearch.="<option value='$value[0]' $search[1]>$value[1]</option>";
		}
		$this->htmlSearch.="</select></span>";
	}
	function data($param)
	{
		$this->SQLquery=$param;
		
	}
	function write($param)
	{
		if (isset($param['urlParam']) and !empty($param['urlParam'])) {
			parse_str($param['urlParam'],$get);
			if ($get['ref']==$this->ref) {
				foreach ($get as $key => $data) {
					$this->$key=$data;
					$this->{"get$key"}=$data;
					if ($key=='sort'||$key=='search') {
						$this->{"get$key"}=explode(',',$data);
					}
				}
			}
		}		
		$this->sort=empty($this->sort)?'':"ORDER BY ".str_replace(',', ' ', $this->sort);
		if (!empty($this->search)) {
			$search=explode(',', $this->search);
			if (!empty($search[0]) && !empty($search[1])) {
				$search[1]="'%$search[1]%'";
				$this->search="having ".implode(' like ', $search);
			}else{
				$this->search='';
			}
			/*if (stripos($this->SQLquery, ' where ')){
				$this->SQLquery=str_ireplace(' where ', " WHERE $this->search AND ", $this->SQLquery);
			}*/
		}
		$this->offset();
		if (isset($this->param['sort'])){$this->sort($this->param['sort']);}
		if (isset($this->param['search'])){$this->search($this->param['search']);}

		$this->SQLquery=$this->SQLquery." $this->search $this->sort";
		$this->totalSize=$this->connector->SQL($this->SQLquery);
		$this->totalSize=count($this->totalSize);

		$this->pagesCenter=ceil($this->pages/2);
		$this->pagesTotal=ceil($this->totalSize/$this->offset);
		$this->page=is_numeric($this->page)?floor($this->page):1;
		if ($this->pagesTotal<$this->page && $this->pagesTotal!=0) {
			$this->page=$this->pagesTotal;
		}elseif ($this->page < 1 && !is_numeric($this->page)) {
			$this->page=1;
		}
		$limit[1]=$this->offset;
		$limit[0]=" limit ".(($this->page-1)*$limit[1]);
		$record=$this->connector->SQL($this->SQLquery." $limit[0] , $limit[1]");
		if ($record > 1){
			$this->write=$param['func']($record);
			if ($this->pagesTotal>1) {
				$this->pageLink();
			}
		}
		// echo $this->SQLquery;
	}
	function pageLink()
	{
		$ps=$this->page<=$this->pagesCenter?1:($this->page>$this->pagesCenter && ($this->page+$this->pages-$this->pagesCenter)<$this->pagesTotal?$this->page-$this->pagesCenter+1:$this->pagesTotal-($this->pages-1));
		$pe=($ps+$this->pages)>$this->pagesTotal?$this->pagesTotal:$ps+$this->pages-1;
		$active=$this->page==1?'active':'';
		$this->pageLink="<a href='1' rel='paging' class='$active'>&lt;&lt;</a>";
		for ($i=$ps; $i <=$pe ; $i++) { 
			$active=$this->page==$i?'active':"";
			$this->pageLink.="<a href='$i' rel='paging' class='$active'>$i</a>";
		}
		$this->pageLink.="<a href='$this->pagesTotal' rel='paging' class='$active'>&gt;&gt;</a>";
	}

	function get($getArray)
	{
		# code...
	}
}
class MLMenu
{
    function __construct()
    {
        $args=func_get_args();
        $this->id=isset($args[3])?$args[3]:'id';
        $this->pid=isset($args[4])?$args[4]:'parent';
        $this->name=isset($args[5])?$args[5]:'name';
        $this->data=array_shift($args);
        $this->option=$args==array()?array(0):$args;
        foreach ($this->data as $k => $v){
         $data[$v[$this->pid]][]=$v;
        }
        array_unshift($this->option, @$data);
    }
    function write($as)
    {
      $this->i=0;
      switch ($as)
        {
            case 'select':
                echo '<select mlmenu name="'.$this->pid.'"  id="'.$this->pid.'" >'.call_user_func_array('self::mdSelect',$this->option).'</select>';
                break;
            case 'list':
            	echo '<ul>'.call_user_func_array('self::mdList',$this->option).'</ul>';
                break;
             case "data":
             	'<ul>'.call_user_func_array('self::mdList',$this->option).'</ul>';
             	break;
        }   
    }
    function dataList()
    {
    	array_unshift($this->dataList, $this->option[1]);
    	return $this->dataList;
    }
    function mdSelect()
    // multi dimension select option    
    {
        $args=func_get_args();
        $a=$args[0];
        $p=isset($args[1])?$args[1]:'';
        $s=isset($args[2])?$args[2]:'';
        $nbsp = str_repeat("&nbsp;", $this->i*4);
        if(isset($a[$p])){
          $html='';$this->i++;
          foreach ($a[$p] as $k => $v) {
          	$this->dataList[]=$v[$this->id];
            $child=self::mdSelect($a,$v[$this->id],$s);
            $selected=$v[$this->id]==$s?'selected':'';
            $html.="<option value='".$v[$this->id]."' $selected>$nbsp".$v[$this->name]."</option>";
              $pname='';
            if ($child) {
              $this->i--;
              $html.=$child;
            }
          }
          
        return $html;
        }
    }
    function mdList()
    // multi dimension list order
    {
        $args=func_get_args();
        $a=$args[0];
        $p=isset($args[1])?$args[1]:'';
        $s=isset($args[2])?$args[2]:'';
        if(isset($a[$p])){
          $html='';$this->i++;
          foreach ($a[$p] as $k => $v) {
          	$this->dataList[]=$v[$this->id];
            $child=self::mdList($a,$v[$this->id],$s);
            $html.="<li data-id='".$v[$this->id]."'>".$v[$this->name]."</li>";
              $pname='';
            if ($child) {
              $this->i--;
              $html.='<ul>'.$child.'</ul>';
            }
          }
          
        return $html;
        }
    }
}

?>