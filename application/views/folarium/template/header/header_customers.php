<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
        <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
            
            <li class="dropdown alert-drp mr-10">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-notifications top-nav-icon"></i><span class="top-nav-icon-badge" id="load_row"></span></a>
                <ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
                    <li>
                        <div class="notification-box-head-wrap">
                            <span class="notification-box-head pull-left inline-block">notifications</span>
                            <!--<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>-->
                            <div class="clearfix"></div>
                            <hr class="light-grey-hr ma-0"/>
                        </div>
                    </li>
                    <li>
                        <div class="streamline message-nicescroll-bar" id="load_data">

                        </div>
                    </li>
                </ul>
            </li>
            
            <li class="dropdown auth-drp">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
                    <img id="preview-upload-logo" src="<?php echo base_url(!empty($sess['users']->users_photo) ? $sess['users']->users_photo : "assets/system/nonuser.png") ?>" alt="user_auth" class="user-auth-img img-circle"/>
                    <span class="user-online-status"></span></a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li>
                        <a href="<?php echo base_url() ?>my-profile"><i class="zmdi zmdi-account"></i><span>Your Account</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>apps-profile"><i class="zmdi zmdi-settings"></i><span>Setting System</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a role="button" style="cursor: pointer" class="logout"><i class="zmdi zmdi-power"></i><span>Signout</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>	
</nav>