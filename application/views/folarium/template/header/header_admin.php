<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="mobile-only-brand pull-left">
        <a id="toggle_nav_btn" class="toggle-left-nav-btn inline-block ml-20 pull-left" href="javascript:void(0);"><i class="zmdi zmdi-menu"></i></a>
        <a id="toggle_mobile_nav" class="mobile-only-view" href="javascript:void(0);"><i class="zmdi zmdi-more"></i></a>
        
        <div id="search_form" role="search" class="top-nav-search collapse pull-left" style="padding-top: 10px;">
            <span style="font-size: 2.1rem;">Javamas Manufacture System</span>
        </div>

    </div>
    <div id="mobile_only_nav" class="mobile-only-nav pull-right">
        <ul class="nav navbar-right top-nav pull-right">
            <li class="dropdown full-width-drp mr-10">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Modul"><i class="zmdi zmdi-apps top-nav-icon"></i></a>
                <ul class="dropdown-menu mega-menu pa-0" data-dropdown-in="slideInRight" data-dropdown-out="flipOutX">
                    <li class="product-nicescroll-bar row">
                        <ul class="pa-20">
                            <li class="col-md-12 col-xs-6 col-menu-list text-left">
                                <h4 style="font-size: 18px"><i class="fa fa-th-large mr-10"></i>MODUL MENU</h4>
                                <hr class="light-grey-hr ma-10 mb-10"/>
                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a href="<?php echo base_url('dashboard') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/profits.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($om) ? $om : 'class="btn btn-default"'; ?> >General Affair</div>
                                </a>

                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a id="set_doc" href="<?php echo base_url('marketing') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/telemarketer.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($doc) ? $doc : 'class="btn btn-default"'; ?>>Marketing</div>
                                </a>
                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a id="set_hr" href="<?php echo base_url('master-data/warehouse') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/stock.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($hr) ? $hr : 'class="btn btn-default"'; ?>>Warehouse</div>
                                </a>

                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a href="<?php echo base_url('manufacture') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/manufacturing.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($bus) ? $bus : 'class="btn btn-default"'; ?>>Manufacture</div>
                                </a>

                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a id="set_acc" href="<?php echo base_url('accounting') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/invoice.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($acc) ? $acc : 'class="btn btn-default"'; ?>>Accounting</div>
                                </a>

                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a href="<?php echo base_url('dashboard/admin-panel') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/pos.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($pro) ? $pro : 'class="btn btn-default"'; ?>>POS Office</div>
                                </a>

                            </li>
                            <li class="col-md-3 col-xs-6 col-menu-list text-center">
                                <a href="<?php echo base_url('master-data/crops') ?>">
                                    <img src="<?php echo base_url() ?>assets/imgs/tumbuhan.png" style="height: 100px"/>
                                    <hr class="light-grey-hr ma-10"/>
                                    <div <?php echo!empty($knw) ? $knw : 'class="btn btn-default"'; ?>>Penyakit tumbuhan</div>
                                </a>

                            </li>
                        </ul>
                    </li>	
                </ul>
            </li>
            <li class="dropdown alert-drp mr-10">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="zmdi zmdi-notifications top-nav-icon"></i><span class="top-nav-icon-badge" id="load_row"></span></a>
                <ul  class="dropdown-menu alert-dropdown" data-dropdown-in="bounceIn" data-dropdown-out="bounceOut">
                    <li>
                        <div class="notification-box-head-wrap">
                            <span class="notification-box-head pull-left inline-block">Notifikasi</span>
                            <!--<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>-->
                            <div class="clearfix"></div>
                            <hr class="light-grey-hr ma-0"/>
                        </div>
                    </li>
                    <li>
                        <div class="streamline message-nicescroll-bar" id="notification-nicescroll-bar">

                        </div>
                    </li>
                </ul>
            </li>
            <li class="dropdown auth-drp">
                <a href="#" class="dropdown-toggle pr-0" data-toggle="dropdown">
                    <img id="preview-upload-logo" src="<?php echo base_url(!empty(session_data('user')['users_photo']) ? session_data('user')['users_photo'] : "assets/imgs/nonuser.png") ?>" alt="user_auth" class="user-auth-img img-circle"/>
                    <span class="user-online-status"></span></a>
                <ul class="dropdown-menu user-auth-dropdown" data-dropdown-in="flipInX" data-dropdown-out="flipOutX">
                    <li>
                        <a href="<?php echo base_url() ?>my-profile"><i class="zmdi zmdi-account"></i><span>Akun Anda</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>settings/apps"><i class="zmdi zmdi-settings"></i><span>Pengaturan Aplikasi</span></a>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <a role="button" style="cursor: pointer" class="logout"><i class="zmdi zmdi-power"></i><span>Keluar</span></a>
                    </li>
                </ul>
            </li>
            <li class="dropdown alert-drp mr-10">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-gear top-nav-icon"></i></a>
                <ul  class="dropdown-menu alert-dropdown">
                    <li>
                        <div class="notification-box-head-wrap">
                            <span class="notification-box-head pull-left inline-block">Pengaturan aplikasi</span>
                            <!--<a class="txt-danger pull-right clear-notifications inline-block" href="javascript:void(0)"> clear all </a>-->
                            <div class="clearfix"></div>
                            <hr class="light-grey-hr ma-0"/>
                        </div>
                    </li>
                    <li>
                        <div class="streamline" id="">
                            <div class="sl-item">
                                <a href="<?php echo base_url('settings/apps/limit-stock') ?>" data-folar-modal="#folar-modal-sm" title="Ubah minimal stok">
                                    <div class="icon bg-green">
                                        <i class="fa fa-cube"></i>
                                    </div>
                                    <div class="sl-content">
                                        <span class="inline-block capitalize-font  pull-left truncate head-notifications">Ganti batas stok</span>
                                        <div class="clearfix"></div>
                                        <p class="truncate">Ganti minimal batas stok.</p>
                                    </div>
                                </a>    
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>	
</nav>