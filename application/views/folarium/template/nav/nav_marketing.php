<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        <li class="mt-20 <?php echo $this->uri->segment(1) == 'marketing' && empty($this->uri->segment(2))? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>"" href="<?php echo base_url('marketing/dashboard'); ?>" ><div class="pull-left"><i class="fa fa-signal mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        </li><li class="navigation-header">
            <span>Master</span> 
            <i class="zmdi zmdi-more"></i>
        </li>

        <li class="mt-10 <?php echo $this->uri->segment(1) == 'marketing' && $this->uri->segment(2) == 'outlet'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/outlet') ?>" ><div class="pull-left"><i class="fa text-white  fa-shopping-bag mr-20"></i><span class="right-nav-text"> Master Toko </span></div><div class="clearfix"></div></a>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Manage Data</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(1) == 'marketing' && $this->uri->segment(2) == 'mg-activity'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('marketing/pre-order') ?>" ><div class="pull-left"><i class="fa text-white  fa-cubes mr-20"></i><span class="right-nav-text"> Activity </span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(1) == 'marketing' && $this->uri->segment(2) == 'pre-order'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('marketing/pre-order') ?>" ><div class="pull-left"><i class="fa text-white  fa-cubes mr-20"></i><span class="right-nav-text"> Purchase Order </span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(1) == 'manage' && $this->uri->segment(2) == 'pre-order' && $this->uri->segment(3) == 'delivery'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage/pre-order/delivery') ?>" ><div class="pull-left"><i class="fa text-white  fa-truck mr-20"></i><span class="right-nav-text"> Manage Pengiriman </span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(2) == 'product' && $this->uri->segment(3) == 'catalog'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" target="_catalog" href="<?php echo base_url('master-data/product/catalog') ?>" ><div class="pull-left"><i class="fa text-white  fa-tags mr-20"></i><span class="right-nav-text"> Katalog Produk</span></div><div class="clearfix"></div></a>
        </li>
    </ul>
</div>