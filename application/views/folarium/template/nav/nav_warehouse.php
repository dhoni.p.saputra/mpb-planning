<div class="fixed-sidebar-left">

    <ul class="nav navbar-nav side-nav nicescroll-bar">
    	<li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && empty($this->uri->segment(5))? 'active' : ''; ?>">
    		<div class="content-center content-distributed" style="flex-direction: column;">
    			<img src="<?php echo base_url('assets/imgs/noimage.jpg'); ?>" class="img-responsive img-circle" style="width: 100px;height: 100px;">
    			<div class="mt-10">
    				<div class="btn-group content-center" role="group" aria-label="...">
						<a class="btn btn-danger btn-action-table btn-xs" href="<?php echo base_url('master-data/warehouse/signout'); ?>"> <i class="fa fa-unlink"> </i> Pindah gudang </a>
						<a class="btn btn-primary btn-action-table btn-xs logout" > <i class="fa fa-unlink"> </i> Sign Out </a>
					</div>
    			</div>
    		</div>

	    </li>    
		<?php if (!is_null( warehouse_active() )): ?>
            <li><hr class="light-grey-hr mb-10"/></li>
            <li>
	        	<a class="<?php echo isset($dash_m) ? $dash_m : null; ?> mt-20" href="<?php echo base_url('warehouse/driver'); ?>" ><div class="pull-left"><i class="fa text-white  fa-bar-chart mr-20"></i><span class="right-nav-text"> Dashboard</span></div><div class="clearfix"></div></a>
            </li>
            
            <li><hr class="light-grey-hr mb-10"/></li>
		    <li class="navigation-header">
		        <span>Bahan</span> 
		        <i class="zmdi zmdi-more"></i>
		    </li>
		   <!-- <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && empty($this->uri->segment(5))? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/bahan'); ?>" ><div class="pull-left"><i class="fa text-white  fa-map-signs mr-20"></i><span class="right-nav-text"> Stock Opname</span></div><div class="clearfix"></div></a>
		    </li> -->
		    <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'masuk'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/bahan/masuk'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-reply mr-20"></i><span class="right-nav-text"> Penerimaan Bahan </span></div><div class="clearfix"></div></a>
		    </li>
		   <!--  <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'keluar'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/keluar'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-forward mr-20"></i><span class="right-nav-text"> Pengeluaran Bahan</span></div><div class="clearfix"></div></a>
		    </li>
		    <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'transfer'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/transfer'); ?>" ><div class="pull-left"><i class="fa text-white  fa-exchange mr-20"></i><span class="right-nav-text"> Transfer Bahan</span></div><div class="clearfix"></div></a>
		    </li> -->
			
            <li><hr class="light-grey-hr mb-10"/></li>
			<li class="navigation-header">
		        <span>Produk</span> 
		        <i class="zmdi zmdi-more"></i>
		    </li>
		   <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && empty($this->uri->segment(5))? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock'); ?>" ><div class="pull-left"><i class="fa text-white  fa-map-signs mr-20"></i><span class="right-nav-text"> Stock Opname</span></div><div class="clearfix"></div></a>
		    </li><!-- 
		    <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'masuk'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/masuk'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-reply mr-20"></i><span class="right-nav-text"> Penerimaan Produk </span></div><div class="clearfix"></div></a>
		    </li> -->
		    <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'keluar'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/keluar'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-forward mr-20"></i><span class="right-nav-text"> Pengeluaran Produk</span></div><div class="clearfix"></div></a>
		    </li>
		    <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'transfer'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/stock/transfer'); ?>" ><div class="pull-left"><i class="fa text-white  fa-exchange mr-20"></i><span class="right-nav-text"> Transfer Produk</span></div><div class="clearfix"></div></a>
		    </li>

            <li><hr class="light-grey-hr mb-10"/></li>
		    <li class="navigation-header">
		        <span>Manage</span> 
		        <i class="zmdi zmdi-more"></i>
		    </li>
		    <li class="mt-10 <?php echo $this->uri->segment(1) == 'warehouse' && $this->uri->segment(2) == 'shipping'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage-data/delivery'); ?>" ><div class="pull-left"><i class="fa text-white  fa-exchange mr-20"></i><span class="right-nav-text"> Manage Deliery</span></div><div class="clearfix"></div></a>
		    </li>
            <li><hr class="light-grey-hr mb-10"/></li>
		    <li class="navigation-header">
		        <span>Monitoring</span> 
		        <i class="zmdi zmdi-more"></i>
		    </li>
		    <li class="mt-10 <?php echo $this->uri->segment(1) == 'warehouse' && $this->uri->segment(2) == 'shipping'? 'active' : ''; ?>">
		        <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('warehouse/shipping'); ?>" ><div class="pull-left"><i class="fa text-white  fa-exchange mr-20"></i><span class="right-nav-text"> Monitoring Shipping</span></div><div class="clearfix"></div></a>
		    </li>

		    
		<?php endif ?>
	</ul>
</div>