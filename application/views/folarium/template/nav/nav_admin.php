<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
        
        <?php if (is_null( warehouse_active() ) && permission_for(array(2))): ?>
            <li class="mt-20">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url(session('roles_redirect')); ?>" ><div class="pull-left"><i class="fa text-white  fa-bar-chart-o mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
            </li>
        <?php endif ?>

        <?php if (permission_for(array(1,3,4))): ?>
            <li class="mt-20">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url(session('roles_redirect')); ?>" ><div class="pull-left"><i class="fa text-white  fa-bar-chart-o mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
            </li>
        <?php endif ?>

        <?php if (is_null( warehouse_active() ) && permission_for(array(2))): ?>
            <li class="navigation-header">
                <span>Data Gudang</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            <?php 
                $whu = warehouse_user();
                if (count($whu) <= 0): 
            ?>
                <li class="mt-10">
                    <span>Anda tidak mempunyai data gudang</span>
                </li>
                
            <?php endif ?>
            <?php foreach ($whu as $key => $value): ?>
                <li class="<?php echo $key > 0? 'mt-10':'' ?> <?php echo $this->uri->segment(4) == 'stock' && empty($this->uri->segment(5))? 'active' : ''; ?>">
                    <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/warehouse/redirect/dashboard/'.md5($value['wh_id'])) ?>" ><div class="pull-left"><i class="fa text-white  fa-home mr-20"></i><span class="right-nav-text"> <?php echo $value['wh_name'] ?></span></div><div class="clearfix"></div></a>
                </li>
            <?php endforeach ?>
            

        <?php endif ?>
        

        <?php if (permission_for(array(1,3,4))): ?>
            <li class="navigation-header">
                <span>Master Data</span> 
                <i class="zmdi zmdi-more"></i>
            </li>

            <li class="<?php echo $this->uri->segment(4) == 'warehouse' || ($this->uri->segment(1) == 'warehouse' && $this->uri->segment(2) == 'dashboard')? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/warehouse'); ?>" ><div class="pull-left"><i class="fa text-white  fa-home mr-20"></i><span class="right-nav-text"> Master Warehouse </span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'users'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/users'); ?>" ><div class="pull-left"><i class="fa text-white  fa-users mr-20"></i><span class="right-nav-text"> Master Pengguna </span></div><div class="clearfix"></div></a>
            </li>

            <li class="mt-10 <?php echo $this->uri->segment(4) == 'material' && empty($this->uri->segment(5))? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/material'); ?>" ><div class="pull-left"><i class="fa text-white  fa-cubes mr-20"></i><span class="right-nav-text"> Master Material </span></div><div class="clearfix"></div></a>
            </li>
            
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'material' && $this->uri->segment(5) == 'category'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/material/category'); ?>" ><div class="pull-left"><i class="fa text-white  fa-tags mr-20"></i><span class="right-nav-text"> Kategori Material </span></div><div class="clearfix"></div></a>
            </li>

            <li class="mt-10 <?php echo $this->uri->segment(4) == 'supplier'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/supplier'); ?>" ><div class="pull-left"><i class="fa text-white  fa-support mr-20"></i><span class="right-nav-text"> Master Supplier </span></div><div class="clearfix"></div></a>
            </li>
            
            

        <?php 
            endif; 
            ?>
        
        <?php if (!is_null( warehouse_active() )): ?>
            <li class="navigation-header">
                <span>Manage stock <?php echo warehouse_active()['wh_name'] ?></span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            
           <li class="<?php echo $this->uri->segment(4) == 'stock' && empty($this->uri->segment(5))? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/stock'); ?>" ><div class="pull-left"><i class="fa text-white  fa-map-signs mr-20"></i><span class="right-nav-text"> Stock Opname</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'masuk'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/stock/masuk'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-reply mr-20"></i><span class="right-nav-text"> Stock Masuk </span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'keluar'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/stock/keluar'); ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-forward mr-20"></i><span class="right-nav-text"> Stock Keluar </span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'stock' && $this->uri->segment(5) == 'transfer'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/stock/transfer'); ?>" ><div class="pull-left"><i class="fa text-white  fa-exchange mr-20"></i><span class="right-nav-text"> Stock Transfer </span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(4) == 'apg' && empty($this->uri->segment(6))? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/apg'); ?>" ><div class="pull-left"><i class="fa text-white  fa-binoculars mr-20"></i><span class="right-nav-text"> APG </span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 ">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?> text-danger" href="<?php echo base_url('admin/manage/view/warehouse/signout'); ?>" ><div class="pull-left"><i class="fa text-white  fa-unlink mr-20"></i><span class="right-nav-text"> Keluar warehouse </span></div><div class="clearfix"></div></a>
            </li>
            
        <?php endif ?>
        <?php if (permission_for(array(1,3,4))): ?>
           <li class="navigation-header">
                <span>Maintenance</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            <li class=" <?php echo $this->uri->segment(6) == 'matrik'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('admin/manage/view/apg/monitoring/matrik'); ?>" ><div class="pull-left"><i class="fa text-white  fa-eye mr-20"></i><span class="right-nav-text"> Monitoring Volume </span></div><div class="clearfix"></div></a>
            </li>
        <?php endif ?>
    </ul>
</div>