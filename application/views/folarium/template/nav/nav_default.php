<style type="text/css">
    .fixed-sidebar-left a[data-toggle="collapse"] .fa
    {
        width: 23px !important;
    }
</style>
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
            <li class="mt-10 <?php echo $this->uri->segment(1) == 'dashboard'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('dashboard') ?>" ><div class="pull-left"><i class="fa text-white  fa-bar-chart mr-20"></i><span class="right-nav-text"> Dashboard</span></div><div class="clearfix"></div></a>
            </li>
        
            <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Master Data</span> 
                <i class="fa fa-ellipsis-h text-white"></i>
            </li><!-- 
            
    </ul>
</div>