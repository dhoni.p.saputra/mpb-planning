

<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        <li class="mt-20 <?php echo $this->uri->segment(1) == 'manufacture' && empty($this->uri->segment(2))? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>"" href="<?php echo base_url('manufacture/dashboard'); ?>" ><div class="pull-left"><i class="fa fa-signal mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
		</li><li class="navigation-header">
		    <span>Master</span> 
		    <i class="zmdi zmdi-more"></i>
		</li>
		<li class="<?php echo $this->uri->segment(1) == 'manufacture' && $this->uri->segment(2) == 'driver'? 'active' : ''; ?>">
			<a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manufacture/driver'); ?>" ><div class="pull-left"><i class="fa text-white  fa-car mr-20"></i><span class="right-nav-text"> Master Sopir</span></div><div class="clearfix"></div></a>
		</li>
		<li class="mt-10 <?php echo $this->uri->segment(2) == 'supplier'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/supplier') ?>" ><div class="pull-left"><i class="fa text-white  fa-truck mr-20"></i><span class="right-nav-text"> Master Pemasok</span></div><div class="clearfix"></div></a>
        </li>

        <li class="mt-10 <?php echo $this->uri->segment(1) == 'master-data' && $this->uri->segment(2) == 'ingredient'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/ingredient') ?>" ><div class="pull-left"><i class="fa text-white  fa-shopping-cart mr-20"></i><span class="right-nav-text"> Master Bahan</span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(2) == 'product-category'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/product-category') ?>" ><div class="pull-left"><i class="fa text-white  fa-cubes mr-20"></i><span class="right-nav-text"> Master Kategori Produk</span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(2) == 'product'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/product') ?>" ><div class="pull-left"><i class="fa text-white  fa-cube mr-20"></i><span class="right-nav-text"> Master Produk</span></div><div class="clearfix"></div></a>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(2) == 'tagline'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/tagline') ?>" ><div class="pull-left"><i class="fa text-white  fa-tags mr-20"></i><span class="right-nav-text"> Master Komposisi</span></div><div class="clearfix"></div></a>
        </li>
        

        <li><hr class="light-grey-hr mb-10"/></li>
        </li><li class="navigation-header">
            <span>Manage</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li class="mt-10 <?php echo $this->uri->segment(1) == 'manage-data' && $this->uri->segment(2) == 'ingredient'? 'active' : ''; ?>">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage-data/ingredient/purchasing') ?>" ><div class="pull-left"><i class="fa text-white  fa-tags mr-20"></i><span class="right-nav-text"> Pemesanan Bahan </span></div><div class="clearfix"></div></a>
        </li>
        
    </ul>
</div>