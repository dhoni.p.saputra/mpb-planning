<div class="fixed-sidebar-left">

    <?php if ($this->uri->segment(1) != 'customer' && $this->uri->segment(2) != 'dashboard'): ?>
        
        <ul class="nav navbar-nav side-nav nicescroll-bar">
            
            <li class="navigation-header">
                <span>Panel & Dashboard</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class="close-outlet <?php echo isset($eom_mg) ? $eom_mg : null; ?>"  href="<?php echo base_url(); ?>"><i class="fa fa-archive mr-20"></i><span class="right-nav-text">Outlet Panel</span></a>
            </li>
            <li>
                <a class="<?php echo isset($eom_mg) ? $eom_mg : null; ?>"  href="<?php echo base_url('admin/outlet/'.$sess['session']['outlet']->outlet_id.'/dashboard'); ?>"><i class="fa fa-area-chart mr-20"></i><span class="right-nav-text">Dashboard</span></a>
            </li>
            <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Lainnya</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class=" text-danger close-outlet <?php echo isset($eom_mg) ? $eom_mg : null; ?>"  href="<?php echo base_url(); ?>"><i class="fa fa-sign-out mr-20"></i><span class="right-nav-text">Keluar Outlet</span></a>
            </li>
            

            <!-- <li><hr class="light-grey-hr mb-10"/></li>
            <li class="navigation-header">
                <span>Laporan</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
            <li>
                <a class="<?php echo isset($pj_lp) ? $pj_lp : null; ?>" href="<?php echo base_url('admin/sale-report'); ?>"><i class="fa fa-dollar mr-20"></i><span class="right-nav-text">Penjualan</span></a>
            </li> -->
        </ul>
    <?php else: ?>
        <ul class="nav navbar-nav side-nav nicescroll-bar outlet-list-box">
            <li class="navigation-header">
                <span>Outlet Aktif</span> 
                <i class="zmdi zmdi-more"></i>
            </li>
        </ul>

    <?php endif; ?>

</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('.feature-box a').on('click', function(){
            Cookies.set('outlet_active', {status:true})
        })
        $('.nicescroll-bar').delegate('.outlet-list','click', function(){
            Cookies.set('outlet_active', {status:true})
        })
        $('.close-outlet').on('click', function(){
            Cookies.remove('outlet_active')
        })

        // you can get this data in modules/dashboard/views/customer.php
        
            if(typeof get_outlet_data == 'function')
            {
                get_outlet_data()
                .done(function(res){
                    res = JSON.parse(res);
                    
                    if(res.show.length > 0)
                    {
                        $('.nicescroll-bar .outlet-list-box .outlet-list').remove();
                    }
        
                    $.each(res.show, function(a,b){
                        if(b.users_outlet_status == 2)
                        {
                            let url = base_url('admin/outlet/'+b.outlet_id+'/dashboard');
                            $('.nicescroll-bar.outlet-list-box').append(`
                                <li>
                                    <a class="outlet-list"  href="`+url+`"><i class="fa fa-shopping-bag mr-20"></i><span class="right-nav-text"> `+b.outlet_name+` </span></a>
                                </li>`
                            )
                        }else
                        {
                            let url = base_url('admin/outlet-manage/features/'+b.outlet_id);
                            $('.nicescroll-bar.outlet-list-box').append(`
                                <li>
                                    <a class="outlet-list"  href="`+url+`" data-folar-modal title="Fitur Kasir"><i class="fa fa-shopping-bag mr-20"></i><span class="right-nav-text"> `+b.outlet_name+` </span></a>
                                </li>`
                            )
                        }
                    })
                })
            }
    })
</script>