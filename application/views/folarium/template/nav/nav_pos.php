<style type="text/css">
    .fixed-sidebar-left a[data-toggle="collapse"] .fa
    {
        width: 23px !important;
    }
</style>
<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">
            <li class="mt-10 <?php echo $this->uri->segment(2) == 'admin-panel' && $this->uri->segment(1) == 'dashboard'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('dashboard/admin-panel') ?>" ><div class="pull-left"><i class="fa text-white  fa-bar-chart mr-20"></i><span class="right-nav-text"> Dashboard</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(1) == 'dashboard' && empty($this->uri->segment(2))? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('dashboard/cashier') ?>" ><div class="pull-left"><i class="fa text-white  fa-usd mr-20"></i><span class="right-nav-text"> Kasir</span></div><div class="clearfix"></div></a>
            </li>
        
            <li class="navigation-header">
                <span>Master Data</span> 
                <i class="fa fa-ellipsis-h text-white"></i>
            </li>

            
            <!-- <li class="mt-10 <?php echo $this->uri->segment(2) == 'supplier'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/supplier') ?>" ><div class="pull-left"><i class="fa text-white  fa-truck mr-20"></i><span class="right-nav-text"> Master Pemasok</span></div><div class="clearfix"></div></a>
            </li> -->
            <li class="mt-10 <?php echo $this->uri->segment(2) == 'member'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/member') ?>" ><div class="pull-left"><i class="fa text-white  fa-user mr-20"></i><span class="right-nav-text"> Master Member</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(2) == 'shelf'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/shelf') ?>" ><div class="pull-left"><i class="fa text-white  fa-list-ol mr-20"></i><span class="right-nav-text"> Master Rak</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(2) == 'target'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('master-data/target') ?>" ><div class="pull-left"><i class="fa text-white  fa-bullseye mr-20"></i><span class="right-nav-text"> Master target</span></div><div class="clearfix"></div></a>
            </li>

             <li class="navigation-header">
                <span>Atur Data</span> 
                <i class="fa fa-ellipsis-h text-white"></i>
            </li>
            <li class="<?php echo $this->uri->segment(1) == 'manage-data' && $this->uri->segment(2) == 'stock'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage-data/stock') ?>" ><div class="pull-left"><i class="fa text-white  fa-cubes mr-20"></i><span class="right-nav-text"> Stok Opname</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(1) == 'manage-data' && $this->uri->segment(2) == 'purchase'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage-data/purchase') ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-forward mr-20"></i><span class="right-nav-text"> Pembelian</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10 <?php echo $this->uri->segment(1) == 'manage-data' && $this->uri->segment(2) == 'expense'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('manage-data/expense') ?>" ><div class="pull-left"><i class="fa text-white  fa-mail-reply mr-20"></i><span class="right-nav-text"> Pengeluaran</span></div><div class="clearfix"></div></a>
            </li>
            <!-- <li class="navigation-header">
                <span>Transaksi</span> 
                <i class="fa fa-ellipsis-h text-white"></i>
            </li>
            <li class="<?php echo $this->uri->segment(1) == 'transaction' && $this->uri->segment(2) == 'sell'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('transaction/sell') ?>" ><div class="pull-left"><i class="fa text-white  fa-shopping-basket mr-20"></i><span class="right-nav-text"> Penjualan</span></div><div class="clearfix"></div></a>
            </li> -->
            <li class="navigation-header">
                <span>Laporan</span> 
                <i class="fa fa-ellipsis-h text-white"></i>
            </li>
            <li class="<?php echo $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'income'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('report/income') ?>" ><div class="pull-left"><i class="fa text-white  fa-credit-card mr-20"></i><span class="right-nav-text"> Pendapatan</span></div><div class="clearfix"></div></a>
            </li>
            <li class="<?php echo $this->uri->segment(1) == 'report' && $this->uri->segment(2) == 'omset'? 'active' : ''; ?>">
                <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>" href="<?php echo base_url('report/omset') ?>" ><div class="pull-left"><i class="fa text-white  fa-credit-card mr-20"></i><span class="right-nav-text"> Laba - Rugi</span></div><div class="clearfix"></div></a>
            </li>
            <li class="mt-10">
                <a href="javascript:void(0);" data-toggle="collapse" data-target="#nav-hutang"><div class="pull-left"><i class="fa fa-money mr-20"></i><span class="right-nav-text">Hutang</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
                <ul id="nav-hutang" class="collapse collapse-level-1">
                    <li>
                        <a href="<?php echo base_url('report/debt/customer') ?>">Hutang Pelanggan</a>
                    </li>
                    <li>
                        <a href="<?php echo base_url('report/debt/supplier') ?>">Hutang Pemasok</a>
                    </li>
                </ul>
            </li>
    </ul>
</div>