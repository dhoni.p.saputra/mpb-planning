<div class="modal fade bs-example-modal-sm" id="folar-modal-sm" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none; overflow-y: auto;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header bg-greensea">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="mySmallModalLabel">Small modal</h5>
            </div>
            <div class="modal-body"> ... </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-example-modal-md" id="folar-modal-md" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none; overflow-y: auto;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header bg-greensea">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="mySmallModalLabel">Small modal</h5>
            </div>
            <div class="modal-body"> ... </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-example-modal-lg" id="folar-modal-lg" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none; overflow-y: auto;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-greensea">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="mySmallModalLabel">Small modal</h5>
            </div>
            <div class="modal-body"> ... </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade bs-example-modal-lgs" id="folar-modal-lgs" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none; overflow-y: auto;">
    <div class="modal-dialog modal-lgs">
        <div class="modal-content">
            <div class="modal-header bg-greensea">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h5 class="modal-title" id="mySmallModalLabel">Small modal</h5>
            </div>
            <div class="modal-body"> ... </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>