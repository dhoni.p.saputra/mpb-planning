<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="icon" type="image/png" href="<?php echo base_url(); ?>assets/img/favicon.png">
    
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title>Javamas<?php echo isset($title)? '- '.$title : ''; ?></title>

        <meta name="author" content="Folarium technomedia">        

            <link href="<?php echo base_url() ?>assets/css/style.css?" rel="stylesheet" type="text/css">

            
        <script src="<?php echo base_url() ?>assets/plugins/jquery/dist/jquery.min.js?"></script>        

        <!-- sweetalert -->
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert2.min.css?">
        <script src="<?php echo base_url() ?>assets/plugins/sweetalert/sweetalert.min.js"></script>        
        
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/plugins/bootstrap/dist/css/bootstrap.min.css?">
        <script src="<?php echo base_url() ?>assets/plugins/bootstrap/dist/js/bootstrap.min.js?"></script>   
        
        <!-- toast -->
        <script src="<?php echo base_url() ?>assets/plugins/toast/toast.js"></script>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/toast/toast.css">
        
        <script src="<?php echo base_url() ?>assets/plugins/moment/min/moment.min.js"></script>

        <link href="<?php echo base_url() ?>assets/css/library_helper.css?" rel="stylesheet" type="text/css">
        
        <!-- prettify -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/plugins/pretty-checkbox/pretty-checkbox.min.css') ?>">

        <?php if (!empty($css)) { ?>
            <?php foreach ($css as $key => $value): ?>
                <link rel="stylesheet" type="text/css" href="<?php echo $value; ?>">
            <?php endforeach ?>
        <?php } ?>
        
        <script>
            var baseUrl = '<?php echo base_url() ?>';
            function base_url(url)
            {
                url = url?url:'';
                url = url.substr(-1) == '/' ? url.substr(0, url.length - 1) : url;
                return '<?php echo base_url() ?>' + url;
            }
            function site_url(url)
            {
                url = url?url:'';
                url = url.substr(-1) == '/' ? url.substr(url.length - 1) : url;
                return '<?php echo site_url() ?>' + url;
            }
        </script> 
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/library_helper/library.helper.js?"></script>
     
    </head>
    
    <body>
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Timeline Dummy</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul class="nav navbar-nav">
                <li class=""><a href="<?php echo base_url('project') ?>">Project <span class="sr-only">(current)</span></a></li>
                
              </ul>
              
              <ul class="nav navbar-nav navbar-right">
                <li><a href="#">Link</a></li>
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Action</a></li>
                    <li><a href="#">Another action</a></li>
                    <li><a href="#">Something else here</a></li>
                    <li role="separator" class="divider"></li>
                    <li><a href="#">Separated link</a></li>
                  </ul>
                </li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>
        <div class="wrapper theme-1-active pimary-color-pink">

            <?php 
                if (isset($nav_side) && $nav_side != false) {
                    $this->load->view($nav_side['content'], @$nav_side['data']);
                }else
                {
                    // $this->load->view("template/nav/nav_default"); 
                }
            ?>
            <div class="<?php echo ( !isset($nav_head) || !isset($nav_side) ) || ($nav_head == false && $nav_side == false)? 'page-wrapper-full' : '' ?> page-wrapper">
                <div class="container-fluid">
                    <?php $this->load->view($content, array('pass' => @$pass)); ?>
                </div>

                
            </div>
        </div>

        <!-- Slimscroll JavaScript -->
        <script src="<?php echo base_url('assets') ?>/js/jquery.slimscroll.js"></script>
    
        <!-- Fancy Dropdown JS -->
        <script src="<?php echo base_url('assets') ?>/js/dropdown-bootstrap-extended.js"></script>
        
        
        <!-- Switchery JavaScript -->
        <script src="<?php echo base_url('assets/plugins') ?>/switchery/dist/switchery.min.js"></script>
        
        <!-- Init JavaScript -->
        <script src="<?php echo base_url('assets') ?>/js/init.js"></script>
        
         

        <link href="<?php echo base_url() ?>assets/plugins/dropify/dist/css/dropify.min.css" rel="stylesheet" type="text/css"/>
        <script src="<?php echo base_url() ?>assets/plugins/dropify/dist/js/dropify.min.js"></script>

        <!-- datatables -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/plugins/datatables/media/css/jquery.dataTables.min.css">
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/datatables/media/js/jquery.dataTables.min.js"></script>

        
        <!-- Bootstrap Daterangepicker JavaScript -->
        <link rel="stylesheet" type="text/css" media="all" href="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.css" />
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/daterangepicker/daterangepicker.js"></script>

        <script src="<?php echo base_url() ?>assets/plugins/jquery-select2/js/select2.min.js"></script>
        <link href="<?php echo base_url() ?>assets/plugins/jquery-select2/css/select2.min.css" rel="stylesheet" type="text/css"/>


        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/jscookie/jscookie.js"></script>    
        <script type="text/javascript" src="<?php echo base_url() ?>assets/plugins/material_design_spinner/material.spinner.min.js"></script>    
       
        
        <?php if (!empty($js)) { ?>
            <?php foreach ($js as $key => $value): ?>
                <script src="<?php echo $value ?>"></script>
            <?php endforeach ?>
        <?php } ?>
            
    </body>
</html>
