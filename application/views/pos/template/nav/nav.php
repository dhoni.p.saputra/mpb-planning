<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        <li class="mt-20">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>"" href="<?php echo base_url('dashboard'); ?>" ><div class="pull-left"><i class="fa fa-signal mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Master Data</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="<?php echo isset($md_m) ? $md_m : 'collapsed'; ?>" href="javascript:void(0);" data-toggle="collapse" data-target="#master_dr" aria-expanded="<?php echo isset($in) ? $in : 'false'; ?>"><div class="pull-left"><i class="zmdi zmdi-menu mr-20"></i><span class="right-nav-text">Master Maintenance</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="master_dr" class="collapse collapse-level-1 <?php echo isset($in) ? $in : null; ?>" aria-expanded="<?php echo isset($in) ? $in : 'false'; ?>">
                <li>
                    <a class="<?php echo isset($tp_sm) ? $tp_sm : null; ?>" href="<?php echo base_url('dash-master/type-maint-master'); ?>">Maintenance Type</a>
                </li>
                <li>
                    <a class="<?php echo isset($src_sm) ? $src_sm : null; ?>"  href="<?php echo base_url('dash-master/source-maint-master'); ?>">Maintenance Source</a>
                </li>
                <li>
                    <a class="<?php echo isset($ts_sm) ? $ts_sm : null; ?>" href="<?php echo base_url('dash-master/type-solve-master'); ?>">Solving Type</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="<?php echo isset($pro_m) ? $pro_m : 'collapsed'; ?>" href="javascript:void(0);" data-toggle="collapse" data-target="#master_drs" aria-expanded="<?php echo isset($ins) ? $ins : 'false'; ?>"><div class="pull-left"><i class="fa fa-dropbox mr-20"></i><span class="right-nav-text">Master Products</span></div><div class="pull-right"><i class="zmdi zmdi-caret-down"></i></div><div class="clearfix"></div></a>
            <ul id="master_drs" class="collapse collapse-level-1 <?php echo isset($ins) ? $ins : null; ?>" aria-expanded="<?php echo isset($ins) ? $ins : 'false'; ?>">
                <li>
                    <a class="<?php echo isset($cprd_sm) ? $cprd_sm : null; ?>" href="<?php echo base_url('dash-master/cat-products-master'); ?>"><span class="right-nav-text">Category</span></a>
                </li>
                <li>
                    <a class="<?php echo isset($prd_sm) ? $prd_sm : null; ?>" href="<?php echo base_url('dash-master/products-master'); ?>"><span class="right-nav-text">Data Products</span></a>
                </li>

            </ul>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>

        <li>
            <a class="<?php echo isset($lctn_sm) ? $lctn_sm : null; ?>" href="<?php echo base_url('dash-master/location-master'); ?>"><i class="fa fa-location-arrow mr-20"></i><span class="right-nav-text">Location</span></a>
        </li>
        <li>
            <a class="<?php echo isset($bnk_sm) ? $bnk_sm : null; ?>" href="<?php echo base_url('dash-master/bank-master'); ?>"><i class="fa fa-bank mr-20"></i><span class="right-nav-text">Bank</span></a>
        </li>
        <li>
            <a class="<?php echo isset($usr_sm) ? $usr_sm : null; ?>" href="<?php echo base_url('dash-master/users-master'); ?>"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Users</span></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Manage</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="<?php echo isset($cs_sm) ? $cs_sm : null; ?>" href="<?php echo base_url('dash-manage/mg-customers'); ?>"><i class="fa fa-users mr-20"></i><span class="right-nav-text">Customers</span></a>
        </li>
        <li>
            <a class="<?php echo isset($prj_sm) ? $prj_sm : null; ?>"  href="<?php echo base_url('dash-manage/mg-projects'); ?>"><i class="fa fa-gavel mr-20"></i><span class="right-nav-text">Projects</span></a>
        </li>
        <li>
            <a class="<?php echo isset($main_sm) ? $main_sm : null; ?>" href="<?php echo base_url('dash-manage/mg-maintenance'); ?>"><i class="fa fa-gears mr-20"></i><span class="right-nav-text">Maintenance</span></a>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Monitoring</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="<?php echo isset($cm_sm) ? $cm_sm : null; ?>" href="<?php echo base_url('dash-monitoring/chart-maintenance'); ?>"><i class="fa fa-calendar-check-o mr-20"></i><span class="right-nav-text">Maint. Periodic</span></a>
        </li>
        <li>
            <a class="<?php echo isset($ca_sm) ? $ca_sm : null; ?>"  href="<?php echo base_url('dash-monitoring/chart-analysis'); ?>"><i class="fa fa-search mr-20"></i><span class="right-nav-text">Maint. Analysis</span></a>
        </li>
    </ul>
</div>