<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        <li class="mt-20">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>"" href="<?php echo base_url('dashboard'); ?>" ><div class="pull-left"><i class="fa fa-signal mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>        
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Manage</span> 
            <i class="zmdi zmdi-more"></i>
        </li>     
        <li>
            <a class="<?php echo isset($prj_sm) ? $prj_sm : null; ?>"  href="<?php echo base_url('dash-manage/mg-projects'); ?>"><i class="fa fa-gavel mr-20"></i><span class="right-nav-text">Projects</span></a>
        </li>
        <li>
            <a class="<?php echo isset($main_sm) ? $main_sm : null; ?>" href="<?php echo base_url('dash-manage/mg-maintenance'); ?>"><i class="fa fa-gears mr-20"></i><span class="right-nav-text">Maintenance</span></a>
        </li>

        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Monitoring</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="<?php echo isset($cm_sm) ? $cm_sm : null; ?>" href="<?php echo base_url('dash-monitoring/chart-maintenance'); ?>"><i class="fa fa-calendar-check-o mr-20"></i><span class="right-nav-text">Maint. Periodic</span></a>
        </li>
        <li>
            <a class="<?php echo isset($ca_sm) ? $ca_sm : null; ?>"  href="<?php echo base_url('dash-monitoring/chart-analysis'); ?>"><i class="fa fa-search mr-20"></i><span class="right-nav-text">Maint. Analysis</span></a>
        </li>
    </ul>
</div>