<div class="fixed-sidebar-left">
    <ul class="nav navbar-nav side-nav nicescroll-bar">

        <li class="mt-20">
            <a class="<?php echo isset($dash_m) ? $dash_m : null; ?>"" href="<?php echo base_url('hr/dashboard'); ?>" ><div class="pull-left"><i class="fa fa-signal mr-20"></i><span class="right-nav-text">Dashboard</span></div><div class="clearfix"></div></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li class="navigation-header">
            <span>Master Data</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li>
            <a class="<?php echo isset($bnk_sm) ? $bnk_sm : null; ?>" href="<?php echo base_url('hr/dash-master/bank-master'); ?>"><i class="fa fa-bank mr-20"></i><span class="right-nav-text">Bank</span></a>
        </li>
        <li>
            <a class="<?php echo isset($usr_sm) ? $usr_sm : null; ?>" href="<?php echo base_url('hr/dash-master/users-master'); ?>"><i class="fa fa-user mr-20"></i><span class="right-nav-text">Users</span></a>
        </li>
        <li>
            <a class="<?php echo isset($dvs_sm) ? $dvs_sm : null; ?>" href="<?php echo base_url('hr/dash-master/division-master'); ?>"><i class="fa fa-cube mr-20"></i><span class="right-nav-text">Division</span></a>
        </li>
        <li>
            <a class="<?php echo isset($pstn_sm) ? $pstn_sm : null; ?>" href="<?php echo base_url('hr/dash-master/position-master'); ?>"><i class="fa fa-flag-checkered mr-20"></i><span class="right-nav-text">Position</span></a>
        </li>
        <li><hr class="light-grey-hr mb-10"/></li>
        <li>
            <a class="<?php echo isset($alw_sm) ? $alw_sm : null; ?>" href="<?php echo base_url('hr/dash-master/allowance-master'); ?>"><i class="fa fa-stop-circle mr-20"></i><span class="right-nav-text">Allowance</span></a>
        </li>
        <li class="navigation-header">
            <span>Manage</span> 
            <i class="zmdi zmdi-more"></i>
        </li>
        <li>
            <a class="<?php echo isset($cntrct_sm) ? $cntrct_sm : null; ?>" href="<?php echo base_url('hr/dash-manage/mg-contract'); ?>"><i class="fa fa-paperclip mr-20"></i><span class="right-nav-text">Contract</span></a>
        </li>
        <li>
            <a class="<?php echo isset($pyrl_sm) ? $pyrl_sm : null; ?>" href="<?php echo base_url('hr/dash-manage/mg-payroll'); ?>"><i class="fa fa-credit-card-alt mr-20"></i><span class="right-nav-text">Payroll</span></a>
        </li>        
    </ul>
</div>