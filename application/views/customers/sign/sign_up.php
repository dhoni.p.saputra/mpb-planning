<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title><?php echo $this->config->item('title') ?></title>
        <meta name="description" content="<?php echo $this->config->item('meta_desc') ?>" />
        <meta name="keywords" content="<?php echo $this->config->item('meta_key') ?>" />
        <meta name="author" content="Folarpos Salepoint"/>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/system/favicon.png">
        <link rel="icon" href="<?php echo base_url(); ?>assets/system/favicon.png" type="image/png">
        <link href="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

        <style>
            body{
                background-image: url('<?php echo base_url() ?>assets/system/bg.jpg');
                background-repeat: no-repeat;
                background-position: center ;
                background-attachment: fixed;
                background-size: cover;
                height: 100%;
                width: auto;
            }
        </style>
        <?php echo $script_captcha; // javascript recaptcha ?>

    </head>
    <body>
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>

        <div class="pa-0">
            <header class="sp-header">
                <div class="sp-logo-wrap pull-left">
                    <a href="<?php echo base_url() ?>">
                        <img class="brand-img mr-10 pull-left" src="<?php echo base_url(); ?>assets/system/favicon.png" style="width: 50px" alt="brand"/>
                        <span class="brand-text" style="float: left">Folarpos Instant</span>
                    </a>
                </div>
                <div class="form-group mb-0 pull-right">
                    <span class="inline-block pr-10">Already have an account?</span>
                    <a class="inline-block btn btn-info  btn-rounded btn-outline" href="<?php echo base_url() ?>signin">Sign In</a>
                </div>
                <div class="clearfix"></div>
            </header>

            <!-- Main Content -->
            <div class="page-wrapper pa-0 ma-0 auth-page" >
                <div class="container-fluid">
                    <!-- Row -->
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="mb-30">
                                            <h3 class="text-center txt-dark mb-10"><i class="fa fa-home mr-10"></i>Sign Up to Customers</h3>
                                            <h6 class="text-center nonecase-font txt-grey">Enter your details below</h6>
                                        </div>	
                                    </div>
                                    <script src="<?php echo base_url() ?>assets/build/jquery-1.10.2.js"></script>
                                    <script src="<?php echo base_url() ?>assets/build/jquery-ui.js"></script>
                                    <link rel="stylesheet" href="<?php echo base_url() ?>assets/build/jquery-ui.css">

                                    <script>
                                        var $j = jQuery.noConflict();
                                        $j(function () {
                                            $j(".datepickers_cust").datepicker(); //Pass the user selected date format                                            
                                        });
                                    </script>
                                    <form method="post" id="frm-reg-cust" action="<?php echo base_url() ?>customers/signup-process">
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-wrap">                                            
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputName_1">Nama Lengkap</label>
                                                    <input type="text" class="form-control" required="" id="exampleInputName_1" placeholder="Nama Lengkap" name="fullname">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputPonsel_3">No. Ponsel</label>
                                                    <input type="text" onkeyup="format_num(this)" class="form-control" required="" id="exampleInputPonsel_3" placeholder="No. Ponsel" name="phone">
                                                </div>

                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_5">Password</label>
                                                    <input type="password" class="form-control" name="password" required="" id="exampleInputpwd_5" placeholder="Password">
                                                </div>
                                            </div>
                                        </div>	
                                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                                            <div class="form-wrap">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputEmail_2">Email</label>
                                                    <input type="email" class="form-control" name="email" required="" id="exampleInputEmail_2" placeholder="Email">
                                                </div>

                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputTanggalLahir_4">Tanggal Lahir</label>
                                                    <input type="text" class="form-control datepickers_cust" name="tanggal_lahir" required="" id="exampleInputTanggalLahir_4" placeholder="Tanggal Lahir">
                                                </div>

                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_6">Confirm Password</label>
                                                    <input type="password" class="form-control" name="conf_password" required="" id="exampleInputpwd_6" placeholder="Ulangi Password">
                                                    <p id="conf_passowrd"></p>
                                                </div>
                                            </div>
                                        </div>	
                                        <div class="col-sm-12 col-xs-12"> 
                                            <div class="fadeIn third captcha">
                                                <?php echo $captcha // tampilkan recaptcha  ?>
                                            </div>
                                            <div class="form-group">
                                                <div class="checkbox checkbox-primary pr-10 pull-left">
                                                    <input id="checkbox_2" required="" type="checkbox">
                                                    <label for="checkbox_2"> I agree to all <span class="txt-primary">Terms</span></label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="form-group text-center">
                                                <button type="submit" class="btn btn-info btn-rounded"><i class="fa fa-sign-in mr-10"></i>sign Up</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-12">
                        <p>2017 &copy; Folarpos Salepoint. All Right Reserved</p>
                    </div>
                </div>
            </footer>
        </div>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>        
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/auth.js"></script>

        <script>



                                                        $.toast().reset('all');
                                                        $("body").removeAttr('class');
                                                        $.toast({
                                                            heading: '<?php echo greetings() ?>',
                                                            text: "Welcome to Form Sign Up - Folarpos Assistant",
                                                            position: 'bottom-right',
                                                            loaderBg: '#fec107',
                                                            icon: 'success',
                                                            hideAfter: 3500,
                                                            stack: 8
                                                        });


        </script>
    </body>
</html>
