<?php if (!isset($_GET['f'])): ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title><?php echo $this->config->item('title') ?></title>
        <meta name="description" content="<?php echo $this->config->item('meta_desc') ?>" />
        <meta name="keywords" content="<?php echo $this->config->item('meta_key') ?>" />
        <meta name="author" content="Folarpos Salepoint"/>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/system/favicon.png">
        <link rel="icon" href="<?php echo base_url(); ?>assets/system/favicon.png" type="image/png">
        <link href="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/modules/slick/slick.css" rel="stylesheet" type="text/css">
        <link href="<?php echo base_url(); ?>assets/bower_components/jquery-toast-plugin/dist/jquery.toast.min.css" rel="stylesheet" type="text/css">

        <meta name="google-signin-client_id" content="591565698145-8obhld4kq6n4j5b0j1j50it9mmsmjau4.apps.googleusercontent.com">
        <style>
            body{
                /*background-image: url('<?php echo base_url() ?>assets/system/bg.jpg');*/
                background-repeat: no-repeat;
                background-position: center ;
                background-attachment: fixed;
                background-size: cover;
                height: 100%;
                width: auto;
            }
            @-webkit-keyframes Gradient {
                0% {
                    background-position: 0% 50%
                }
                50% {
                    background-position: 100% 50%
                }
                100% {
                    background-position: 0% 50%
                }
            }

            @-moz-keyframes Gradient {
                0% {
                    background-position: 0% 50%
                }
                50% {
                    background-position: 100% 50%
                }
                100% {
                    background-position: 0% 50%
                }
            }

            @keyframes Gradient {
                0% {
                    background-position: 0% 50%
                }
                50% {
                    background-position: 100% 50%
                }
                100% {
                    background-position: 0% 50%
                }
            }

            .overlay-page-wrapper--animation
            {
                background: linear-gradient(-45deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
                background-size: 400% 400%;
                /*opacity: .8;*/
                -webkit-animation: Gradient 15s ease infinite;
                -moz-animation: Gradient 15s ease infinite;
                animation: Gradient 15s ease infinite;
                z-index: 3;
                position: absolute;
                height: 100%;
                width: 100%;
            }
            .background-overlay
            {
                background-size: cover; 
                background-repeat: no-repeat; 
                background-position: center; 
                background-image: url(<?php echo base_url('assets/img').'/login_bg.jpg' ?>);
                position: absolute; left: 0px; top: 0px;width: 100%;height: 100vh;z-index: 2;filter: blur(5px);
            }
            .abcRioButtonLightBlue
            {
                width: 100% !important;
            }
        </style>
    </head>
    <body >
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>

        <div class="pa-0">

            <div class="page-wrapper pa-0 ma-0 auth-page" style="position: relative; height: 100vh;">
                <!-- <div class="background-overlay" style=""></div> -->
                <div class="overlay-page-wrapper--animation" style=" ""></div>
                <div style="position:absolute;z-index: 4; width: 100%; height: 100vh; overflow: none; display: flex; justify-content: center; align-items: center; background-color: rgba(0,0,0,.5);">
                    
                    <div class="row" style="" id="login-box">
                        <div class="col-md-8" style="height: 100%; background-color: rgba(0,0,0,.6);">
                            <div class="overlay-banner" style="height: 100%; width: 100%;display: flex; align-items: center; justify-content: center;">
                                <div class="slicking">
                                    <img src="<?php echo base_url('assets/img').'/folarpos_instan_illustrate.png' ?>" alt="Folarpos Instant" style="width: 90%;">                            
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 login-box--form" style="" id="page-wrapper-exchange">
                        <?php endif ?>
                            <div class="form-group mb-0 pull-right" style="height: 20%;">
                                <span class="inline-block pr-10">Have an account?</span>
                                <a class="inline-block btn btn-info  btn-rounded btn-outline" href="<?php echo base_url() ?>signin" onclick="return load_signin(event)" style="padding: 6px 19px;">Sign in</a>
                            </div>
                            <div class="" style="height: 80%;">
                                <!-- Row -->
                                <div class=" ">
                                    <div class="">
                                        <div class="ml-auto mr-auto no-float">
                                            <div class="row" >
                                                <div class="col-sm-12 col-xs-12 col-md-12 col-lg-12">

                                                    <div class="mb-30">
                                                        <center><img class="brand-img mr-10" src="<?php echo base_url(); ?>assets/system/favicon/apple-icon-180x180.png" style="width: 100px" alt="brand"/></center>
                                                        <h3 class="text-center txt-dark mb-10" style="font-size: 1.8em;"><b><span>Welcome to</span> <span class="text-main">Folarpos</span></b> </h3>
                                                    </div>  
                                                    <div class="form-wrap" id="form-wrap">

                                            <form id="frm-reg-cust" action="<?php echo base_url() ?>/signup-process">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputName_1">Fullname</label>
                                                    <input type="text" name="fullname" class="form-control" required="" id="exampleInputName_1" placeholder="Fullname">
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                                    <input type="email" name="email" class="form-control" required="" id="exampleInputEmail_2" placeholder="Enter email">
                                                </div>
                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_2">Password</label>
                                                    <input type="password" name="password" class="form-control" required="" id="exampleInputpwd_2" placeholder="Enter pwd">
                                                </div>
                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_3">Confirm Password</label>
                                                    <input type="password" name="password_conf" class="form-control" required="" id="exampleInputpwd_3" placeholder="Enter pwd">
                                                </div>
                                                <div class="form-group">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_4">Darimanakah anda mengetahui tentang kami?</label>
                                                    <select class="form-control" name="cust_ref_id" onchange="change_ref(this)">
                                                        <option value="0">--- Isikan pilihan ---</option>
                                                        <?php foreach ($reference as $key => $value): ?>                                                        
                                                            <option data-text="<?php echo $value['cust_ref_name'] ?>" value="<?php echo $value['cust_ref_id'] ?>"><?php echo $value['cust_ref_name'] ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                                <div class="form-group sr-only ref_value">
                                                    <label class="pull-left control-label mb-10" for="exampleInputpwd_3">Isikan sumber informasi</label>
                                                    <input type="text" name="cust_ref_value" class="form-control" required="" id="exampleInputpwd_5" placeholder="Isikan sumber informasi...">
                                                </div>
                                                <div class="form-group">
                                                    <div class="checkbox checkbox-primary pr-10 pull-left">
                                                        <input id="checkbox_2" required="" type="checkbox">
                                                        <label for="checkbox_2"> I agree to all <span class="txt-primary">Terms</span></label>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="form-group text-center">
                                                    <button type="submit" class="btn btn-info btn-rounded"><i class="fa fa-sign-in mr-10"></i> <span class="text">sign Up</span> <span class="fa fa-spin fa-cog sr-only"></span> </button>
                                                </div>
                                                <div class="g-signin2" data-width="300" data-height="50" data-longtitle="true" style="margin-bottom: 20px;"></div>
                                            </form>
                                                
 </div>
                                                </div>  
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                                            <?php if (!isset($_GET['f'])): ?>
                    </div>
                </div>

            </div>
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-12">
                        <p>2017 &copy; Folarpos Salepoint. All Right Reserved</p>
                    </div>
                </div>
            </footer>
        </div>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery-toast-plugin/dist/jquery.toast.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/auth.js"></script>
                                            <?php endif ?>
        <script>
            function load_signin(e)
            {
                e.preventDefault();
                let url = $(e.target).attr('href')+'?f';
                $('#page-wrapper-exchange').load(url, {}, ()=>{
                    history.pushState({}, "Folarium Sign up", url);
                })

                return false;
            }

            function onSuccess(googleUser) {
              console.log('Logged in as: ' + googleUser.getBasicProfile().getName());
            }
            function onFailure(error) {
              console.log(error);
            }
            function renderButton() {
                let w = $('#frm-login').css('width')
              gapi.signin2.render('my-signin2', {
                'scope': 'profile email',
                'width': w,
                'height': 50,
                'longtitle': true,
                'theme': 'dark',
                'onsuccess': onSuccess,
                'onfailure': onFailure
              });
            }

            function change_ref(e)
            {
                let val = $(e).val();
                if(val < 1)
                {
                    $('.ref_value').addClass('sr-only')

                }else{
                    $('.ref_value').removeClass('sr-only')
                }
            }
            $("body").removeAttr('class');
            
        </script>
        <script src="https://apis.google.com/js/platform.js?onload=renderButton" async defer></script>
    </body>
</html>
