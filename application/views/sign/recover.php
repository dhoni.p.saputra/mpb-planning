<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <title><?php echo $this->config->item('title') ?></title>
        <meta name="description" content="<?php echo $this->config->item('meta_desc') ?>" />
        <meta name="keywords" content="<?php echo $this->config->item('meta_key') ?>" />
        <meta name="author" content="Folarpos Salepoint"/>
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/system/favicon.png">
        <link rel="icon" href="<?php echo base_url(); ?>assets/system/favicon.png" type="image/png">
        <link href="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/css/jasny-bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet" type="text/css">
        <style>
            body{
                background-image: url('<?php echo base_url() ?>assets/system/bg.jpg');
                background-repeat: no-repeat;
                background-position: center ;
                background-attachment: fixed;
                background-size: cover;
                height: 100%;
                width: auto;
            }
        </style>
    </head>
    <body>
        <div class="preloader-it">
            <div class="la-anim-1"></div>
        </div>

        <div class="pa-0">
            <div class="page-wrapper pa-0 ma-0 auth-page">
                <div class="container-fluid">
                    <!-- Row -->
                    <div class="table-struct full-width full-height">
                        <div class="table-cell vertical-align-middle auth-form-wrap">
                            <div class="auth-form  ml-auto mr-auto no-float">
                                <div class="row">
                                    <div class="col-sm-12 col-xs-12">
                                        <div class="sp-logo-wrap text-center pa-0 mb-30">
                                            <a href="<?php echo base_url() ?>">
                                                <img class="brand-img mr-10" src="<?php echo base_url() ?>assets/system/favicon.png" style="width: 50px" alt="brand"/>
                                                <span class="brand-text">Folarpos Instant</span>
                                            </a>
                                        </div>
                                        <div class="mb-30">
                                            <h3 class="text-center txt-dark mb-10">Need help with your password?</h3>
                                            <h6 class="text-center txt-grey nonecase-font">Enter the email you use for Folarpos Instant, and we’ll help you create a new password.</h6>
                                        </div>	
                                        <div class="form-wrap">
                                            <form action="#">
                                                <div class="form-group">
                                                    <label class="control-label mb-10" for="exampleInputEmail_2">Email address</label>
                                                    <input type="email" class="form-control" required="" id="exampleInputEmail_2" placeholder="Enter email">
                                                </div>

                                                <div class="form-group text-center">

                                                    <button type="submit" class="btn btn-primary btn-rounded"><i class="fa fa-send mr-10"></i>Reset</button>

                                                </div>
                                                <a href="<?php echo base_url() ?>" class="pull-right"><i class="fa fa-home mr-10"></i>Back to Signin</a>
                                            </form>
                                        </div>
                                    </div>	
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /Row -->	
                </div>

            </div>
            <footer class="footer container-fluid pl-30 pr-30">
                <div class="row">
                    <div class="col-sm-12">
                        <p>2017 &copy; Folarpos Salepoint. All Right Reserved</p>
                    </div>
                </div>
            </footer>
        </div>
        <script src="<?php echo base_url(); ?>assets/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/bower_components/jasny-bootstrap/dist/js/jasny-bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.slimscroll.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
    </body>
</html>
