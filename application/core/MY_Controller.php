<?php

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function authentication_root() {
        if (!empty($this->session->userdata(session_config_name()))) {
            $data['session'] = $this->session->userdata(session_config_name());            
            $data['users'] = $this->db->query("select * from md_users u join md_roles r on r.roles_id = u.roles_id where md5(u.users_id) = '" . $data['session']['user']['users_id'] . "'")->last_row();
            return $data;
        } else {
            redirect("");
        }
    }
    
     public function user_session($module, $view, $data = FALSE) {
        $this->load->view($module . '/' . $view, $data);
    }

    public function apps() {
        $data['app'] = $this->db->query("select * from apps limit 1")->last_row();
        return $data;
    }

    public function send_mail($msg, $type) {
        if (!empty($msg)) {
            $this->load->library('email');
            $config['protocol'] = 'smtp';
            $config['mailpath'] = '/usr/sbin/sendmail';
            $config['charset'] = 'iso-8859-1';
            $config['smtp_host'] = '@folarpos.co.id';
            $config['smtp_user'] = 'mail@@folarpos.co.id';
            $config['smtp_pass'] = 'm41l4p412t7kt';
            $config['smtp_port'] = '587';
            $config['mailtype'] = 'html';
            $config['wordwrap'] = TRUE;

            $this->email->initialize($config);
            $this->email->from('subscribes@folarpos.co.id', 'Folarpos Instant (Subscribes)');
            $this->email->to($msg['to']);
            if (!empty($exe_incl['cc'])) {
                $this->email->cc($msg['cc']);
            }
            $this->email->subject($msg['title']);
            $this->email->message($msg['desc']);

            $kirim = $this->email->send();
            if ($kirim) {

                $histor['subs_id'] = $msg['subs_id'];
                $histor['subs_send_page'] = $msg['url_pages'];
                $histor['subs_send_date'] = date('Y-m-d H:i:s');

                $this->crud_model->insert_data("mg_subs_send", $histor);
            }
        }
    }

}
